package com.creatur.data.question

import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import retrofit2.http.Body
import retrofit2.http.Header
import retrofit2.http.POST
import java.util.concurrent.TimeUnit

private const val BASE_URL =
    "https://creatur-backend-dev.herokuapp.com/"

private val moshi = Moshi.Builder()
    .add(KotlinJsonAdapterFactory())
    .build()

private val okHttpClient = OkHttpClient.Builder()
    .connectTimeout(20, TimeUnit.SECONDS)
    .writeTimeout(20, TimeUnit.SECONDS)
    .readTimeout(30, TimeUnit.SECONDS)
    .build()

private val retrofit = Retrofit.Builder()
    .client(okHttpClient)
    .addConverterFactory(MoshiConverterFactory.create(moshi))
    .baseUrl(BASE_URL)
    .build()

/* This class is used to translate the Fake DB information into a Kotlin Object, so that the instances of the program can understand and manipulate it */
interface ResponseService {
    @POST("users/profiling")
    suspend fun profiling(@Header("Authorization") token: String, @Body response: Response): Message
}

object ResponseCall {
    val retrofitService : ResponseService by lazy {
        retrofit.create(ResponseService::class.java) }
}