package com.creatur.data.question

class ResponseRepository {
    suspend fun profiling(token: String, response: Response): Message {
        try {
            return ResponseCall.retrofitService.profiling(token, response)
        } catch (e: Exception) {
            throw e
        }
    }
}