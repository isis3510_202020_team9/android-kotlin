package com.creatur.data.question

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class Response(val responses: List<Question>) {
}

