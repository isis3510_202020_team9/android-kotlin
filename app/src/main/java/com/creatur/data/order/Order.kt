package com.creatur.data.order

/* Model that represents an Order object */
data class Order(val productsInfo: List<TmpProduct>, val ventureId: String, val address: String, val desiredDate: String){

}

data class TmpProduct(val id: String?, val units: Int?)