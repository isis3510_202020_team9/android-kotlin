package com.creatur.data.order

import android.os.Parcelable
import com.creatur.data.product.Product
import com.creatur.data.venture.Venture
import kotlinx.android.parcel.Parcelize

@Parcelize
data class OrderStatus (
        val desiredDate: String,
        val address: String,
        val finalPrize: Double,
        var venture: Venture? = null,
        val id: String,
        var review: Review? = null,
        val products: List<tmpprod>? = null,
        var status: String,
        val createdAt: String): Parcelable

@Parcelize
data class tmpprod (
        val id: String,
        val units: Int,
        val product: Product
): Parcelable

@Parcelize
data class StatusUpdate(
        val status: String
): Parcelable

@Parcelize
data class Review(
        val grade: Int,
        val orderId: String? = null
): Parcelable