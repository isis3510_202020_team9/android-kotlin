package com.creatur.data.order

class OrderRepository {
    suspend fun buy(token: String, order: Order): OrderStatus {
        try {
            return OrderCall.retrofitService.buy(token, order)
        }
        catch (e: Exception){
            throw e
        }
    }

    suspend fun updateStatus(token: String, orderId: String, status: StatusUpdate): OrderStatus {
        try {
            return OrderCall.retrofitService.updateStatus(token, orderId, status)
        }
        catch (e: Exception){
            throw e
        }
    }

    suspend fun review(token: String, review: Review) {
        try {
            return OrderCall.retrofitService.review(token, review)
        }
        catch (e: Exception){
            throw e
        }
    }
}