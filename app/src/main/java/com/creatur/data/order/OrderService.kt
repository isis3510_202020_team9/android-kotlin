package com.creatur.data.order

import com.creatur.data.product.ProductService
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import retrofit2.http.*
import java.util.concurrent.TimeUnit

private const val BASE_URL =
    "https://creatur-backend-dev.herokuapp.com/"

private val moshi = Moshi.Builder()
    .add(KotlinJsonAdapterFactory())
    .build()

private val okHttpClient = OkHttpClient.Builder()
    .connectTimeout(20, TimeUnit.SECONDS)
    .writeTimeout(20, TimeUnit.SECONDS)
    .readTimeout(30, TimeUnit.SECONDS)
    .build()

private val retrofit = Retrofit.Builder()
    .client(okHttpClient)
    .addConverterFactory(MoshiConverterFactory.create(moshi))
    .baseUrl(BASE_URL)
    .build()

interface OrderService {
    @POST("products/buy")
    suspend fun buy(@Header("Authorization") token: String, @Body order: Order): OrderStatus

    @PUT("ventures/orders/{orderId}/status")
    suspend fun updateStatus(@Header("Authorization") token: String, @Path("orderId") orderid : String, @Body status: StatusUpdate): OrderStatus

    @POST("reviews")
    suspend fun review(@Header("Authorization") token: String, @Body review: Review)
}

object OrderCall {
    val retrofitService : OrderService by lazy {
        retrofit.create(OrderService::class.java) }
}