package com.creatur.data.product

import android.os.Parcelable
import com.creatur.data.venture.Venture
import kotlinx.android.parcel.Parcelize
@Parcelize
data class Product(
        val id: String? = null,
        val name: String,
        val prize: Int,
        val description: String,
    //val units: Int,
        var photo: String? = null,
        val productCategory: String,
        val venture: Venture? = null
    //val tags: String
) : Parcelable