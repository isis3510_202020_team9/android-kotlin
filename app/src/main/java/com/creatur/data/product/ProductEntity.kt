package com.creatur.data.product
import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.ColumnInfo
import kotlin.random.Random

@Entity(tableName = "products_table")
data class ProductEntity (
    @PrimaryKey
    val id: String,
    @ColumnInfo(name = "product_name")
    val name: String,
    @ColumnInfo(name = "product_price")
    val prize: Int,
    @ColumnInfo(name = "product_description")
    val description: String,
   // @ColumnInfo(name = "product_units")
    //val units: Int,
    @ColumnInfo(name = "product_photo")
    val photo: String,
    @ColumnInfo(name = "product_category")
    val productCategory: String)

fun List<ProductEntity>.asDomainModel(): List<Product> {
    return map {
        Product(
            id = it.id,
            name = it.name,
            prize = it.prize,
            description = it.description,
           // units = it.units,
            photo = it.photo,
            productCategory = it.productCategory)
    }
}

fun List<Product>.asDatabaseModel(): List<ProductEntity> {
    return map {
        ProductEntity(
            id = it.id?: Random.nextInt(0,847556).toString(),
            name = it.name,
            prize = it.prize,
            description = it.description,
            //units = it.units,
            photo = it.photo?: Random.nextInt(0,847556).toString(),
            productCategory = it.productCategory)
    }
}

