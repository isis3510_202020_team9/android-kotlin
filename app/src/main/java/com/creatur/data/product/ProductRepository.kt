package com.creatur.data.product

import android.util.Log
import com.creatur.data.DatabaseLocal
import java.lang.Exception

class ProductRepository() {
    lateinit var productDAO: ProductDAO

    suspend fun suggest(token: String): List<Product> {
        try {
            val lista: List<Product> = ProductCall.retrofitService.suggested(token)
            productDAO.insertAll(lista.asDatabaseModel())
            return lista
        } catch (e: Exception){
            throw e
        }
    }

    suspend fun create(token: String, product: Product): Product{
        try {
            return ProductCall.retrofitService.create(token, product)
        } catch (e: Exception){
            throw e
        }
    }
    
    suspend fun productDetail(token: String, ventureId: String): List<Product> {
        try {
            val lista: List<Product> = ProductCall.retrofitService.ventureDetail(ventureId, token)
            //productDAO.insertAll(lista.asDatabaseModel())
            return lista
        } catch (e: Exception){
            throw e
        }
    }

    suspend fun productCategory(token: String, category: String): List<Product>? {
        try {
            val lista: List<Product>? = ProductCall.retrofitService.category(token, category)
            //productDAO.insertAll(lista.asDatabaseModel())
            return lista
        } catch (e: Exception){
            throw e
        }
    }
}