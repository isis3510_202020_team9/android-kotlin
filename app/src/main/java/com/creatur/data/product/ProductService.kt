package com.creatur.data.product

import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.POST
import retrofit2.http.Path
import java.util.concurrent.TimeUnit

private const val BASE_URL =
    "https://creatur-backend-dev.herokuapp.com/"

private val moshi = Moshi.Builder()
    .add(KotlinJsonAdapterFactory())
    .build()

private val okHttpClient = OkHttpClient.Builder()
    .connectTimeout(20, TimeUnit.SECONDS)
    .writeTimeout(20, TimeUnit.SECONDS)
    .readTimeout(30, TimeUnit.SECONDS)
    .build()

private val retrofit = Retrofit.Builder()
    .client(okHttpClient)
    .addConverterFactory(MoshiConverterFactory.create(moshi))
    .baseUrl(BASE_URL)
    .build()

interface ProductService {
    @GET("products/suggested?page=1")
    suspend fun suggested(@Header("Authorization") token: String): List<Product>

    @POST("ventures/products")
    suspend fun create(@Header("Authorization") token: String, @Body product: Product): Product

    @GET("ventures/{ventureId}/products")
    suspend fun ventureDetail(@Path("ventureId") ventureid : String , @Header("Authorization")token: String): List<Product>

    @GET("products/category/{category}")
    suspend fun category(@Path("category")category : String , @Header("Authorization") token: String): List<Product>?
}

object ProductCall {
    val retrofitService : ProductService by lazy {
        retrofit.create(ProductService::class.java) }
}