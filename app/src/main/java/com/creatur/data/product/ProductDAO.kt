package com.creatur.data.product

import androidx.lifecycle.LiveData
import androidx.room.*
import com.creatur.data.product.ProductEntity

@Dao
interface ProductDAO {
    @Insert
    fun insert(product: ProductEntity)
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(producs: List<ProductEntity>)
    @Update
    fun update (product: ProductEntity)
    @Query("SELECT * from products_table WHERE id = :key")
    fun get(key: Long): ProductEntity?
    @Query("SELECT * from products_table")
    fun getAll(): LiveData<List<ProductEntity?>>
    @Query("SELECT * from products_table ORDER BY id DESC LIMIT 10")
    fun getRecentTen(): LiveData<List<ProductEntity?>>

}