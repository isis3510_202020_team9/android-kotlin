package com.creatur.data

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.creatur.data.product.ProductDAO
import com.creatur.data.product.ProductEntity
import com.creatur.data.venture.VentureDAO
import com.creatur.data.venture.VentureEntity

@Database(entities = [VentureEntity::class, ProductEntity:: class], version = 1, exportSchema = false)
abstract class DatabaseLocal : RoomDatabase() {

    abstract val ventureDao: VentureDAO
    abstract val productDao: ProductDAO

    companion object {

        @Volatile
        private var INSTANCE: DatabaseLocal? = null

        fun getInstance(context: Context): DatabaseLocal {
            synchronized(this) {
                var instance = INSTANCE

                if (instance == null) {
                    instance = Room.databaseBuilder(
                        context.applicationContext,
                        DatabaseLocal::class.java,
                        "local_database"
                    )
                        .fallbackToDestructiveMigration()
                        .build()
                    INSTANCE = instance
                }
                return instance
            }
        }
    }
}