package com.creatur.data.venture

import android.os.Parcelable
import com.squareup.moshi.Json
import kotlinx.android.parcel.Parcelize
@Parcelize
data class Venture(
    val id: String? = null,
    val name: String,
    val phone: String,
    val address: String,
    val facebookUrl: String? = null,
    val instagramUrl: String? = null,
    val latitude: Double? = null,
    val longitude: Double? = null,
    val photo: String? = null,
    val categories: List<String>? = null
) : Parcelable

@Parcelize
data class VentureResp(
        val id: String? = null,
        val name: String,
        val phone: String,
        val address: String,
        val facebookUrl: String? = null,
        val instagramUrl: String? = null,
        val latitude: Double? = null,
        val longitude: Double? = null,
        val photo: String? = null
) : Parcelable