package com.creatur.data.venture

import com.creatur.data.order.Order
import com.creatur.data.order.OrderStatus
import com.creatur.data.product.Product
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import retrofit2.http.*
import java.util.concurrent.TimeUnit

private const val BASE_URL =
    "https://creatur-backend-dev.herokuapp.com/"

private val moshi = Moshi.Builder()
    .add(KotlinJsonAdapterFactory())
    .build()

private val okHttpClient = OkHttpClient.Builder()
    .connectTimeout(20, TimeUnit.SECONDS)
    .writeTimeout(20, TimeUnit.SECONDS)
    .readTimeout(30, TimeUnit.SECONDS)
    .build()

private val retrofit = Retrofit.Builder()
    .client(okHttpClient)
    .addConverterFactory(MoshiConverterFactory.create(moshi))
    .baseUrl(BASE_URL)
    .build()

interface VentureService {
    @GET("ventures/suggested")
    suspend fun suggested(@Header("Authorization") token: String): List<Venture>

    @GET("ventures/near")
    suspend fun near(@Query("latitude") latitude: Double, @Query("longitude") longitude: Double, @Header("Authorization") token: String): Near

    @POST("ventures/start")
    suspend fun start(@Header("Authorization") token:String, @Body venture: Venture): VentureResp

    @GET("ventures/category/{category}")
    suspend fun category(@Path("category")category : String , @Header("Authorization") token: String): List<Venture>?

    @GET("users/my-venture")
    suspend fun myVenture(@Header("Authorization") token:String): Venture

    @GET("ventures/{ventid}/products")
    suspend fun ventureProducts(@Header("Authorization") token:String, @Path("ventid") ventureid: String): List<Product>

    @GET("ventures/orders")
    suspend fun ventureOrders(@Header("Authorization") token: String): List<OrderStatus>

    @GET("users/orders")
    suspend fun userOrders(@Header("Authorization") token: String): List<OrderStatus>
}

object VentureCall {
    val retrofitService : VentureService by lazy {
        retrofit.create(VentureService::class.java) }
}