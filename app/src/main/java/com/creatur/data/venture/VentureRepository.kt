package com.creatur.data.venture

import com.creatur.data.order.Order
import com.creatur.data.order.OrderStatus
import com.creatur.data.product.Product
import java.lang.Exception

class VentureRepository {
    suspend fun near(token: String, lat: Double, lon: Double): Near {
        try {
            return VentureCall.retrofitService.near(lat, lon, token)
        } catch (e: Exception){
            throw e
        }
    }

    suspend fun suggest(token: String): List<Venture> {
        try {
            return VentureCall.retrofitService.suggested(token)
        } catch (e: Exception){
            throw e
        }
    }

    suspend fun startVenture(token: String, venture: Venture): VentureResp {
        try {
            return VentureCall.retrofitService.start(token, venture)
        } catch (e: Exception){
            throw e
        }
    }

    suspend fun ventureCategory(token: String, category: String): List<Venture>? {
        try {
            return VentureCall.retrofitService.category(token, category)
        } catch (e: Exception){
            throw e
        }
    }

    suspend fun myvent(token: String): Venture {
        try {
            return VentureCall.retrofitService.myVenture(token)
        } catch (e: Exception){
            throw e
        }
    }

    suspend fun getVentureOrders(token: String): List<OrderStatus> {
        try {
            return VentureCall.retrofitService.ventureOrders(token)
        } catch (e: Exception){
            throw e
        }
    }

    suspend fun getVentureProducts(token: String, ventid: String): List<Product> {
        try {
            return VentureCall.retrofitService.ventureProducts(token, ventid)
        } catch (e: Exception){
            throw e
        }
    }

    suspend fun getUserOrders(token: String): List<OrderStatus> {
        try {
            return VentureCall.retrofitService.userOrders(token)
        } catch (e: Exception){
            throw e
        }
    }
}