package com.creatur.data.venture

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Update

@Dao
interface VentureDAO {
    @Insert
    fun insert(venture: VentureEntity)
    @Update
    fun update (venture: VentureEntity)
    @Query("SELECT * from ventures_table WHERE id = :key")
    fun get(key: Long): VentureEntity?
    @Query("SELECT * from ventures_table")
    fun getAll(): LiveData<List<VentureEntity?>>
    @Query("SELECT * from ventures_table ORDER BY id DESC LIMIT 10")
    fun getRecentTen(): LiveData<List<VentureEntity?>>

}