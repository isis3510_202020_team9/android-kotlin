package com.creatur.data.venture

import com.creatur.data.product.Product
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class Near(@field:Json(name = "nearVentures") val nearVentures : List<Venture>, @field:Json(name = "nearProducts") val nearProducts : List<Product>){
}