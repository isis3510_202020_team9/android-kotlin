package com.creatur.data.venture

import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.ColumnInfo
@Entity(tableName = "ventures_table")
data class VentureEntity (
    @PrimaryKey
    val id: String,
    @ColumnInfo(name = "venture_name")
    val name: String,
    @ColumnInfo(name = "venture_phone")
    val phone: String,
    @ColumnInfo(name = "venture_address")
    val address: String,
    @ColumnInfo(name = "venture_facebookUrl")
    val facebookUrl: String,
    @ColumnInfo(name = "venture_instagramUrl")
    val instagramUrl: String,
    @ColumnInfo(name = "venture_latitude")
    val latitude: Double,
    @ColumnInfo(name = "venture_longitude")
    val longitude: Double,
    @ColumnInfo(name = "venture_photo")
    val photo: String)