package com.creatur.data.login

import com.squareup.moshi.JsonClass

/* Model that represents a Token object */
@JsonClass(generateAdapter = true)
data class Token(val token: String, val role: String){}