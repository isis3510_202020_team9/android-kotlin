package com.creatur.data.login

class LoginRegisterRepository{
    suspend fun login(loginRegister: LoginRegister): Token {
        try {
            return LoginRegisterCall.retrofitService.login(loginRegister)
        } catch (e: Exception) {
            throw e
        }
    }

    suspend fun register(loginRegister: LoginRegister): Token {
        try {
            return LoginRegisterCall.retrofitService.register(loginRegister)
        } catch (e: Exception) {
            throw e
        }
    }
}