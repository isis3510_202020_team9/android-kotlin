package com.creatur.data.login

/* Model that represents a Login or a Register object */
data class LoginRegister(val email: String, val password: String){

}