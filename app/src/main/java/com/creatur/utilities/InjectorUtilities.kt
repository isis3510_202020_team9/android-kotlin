package com.creatur.utilities

import android.content.Context
import androidx.lifecycle.ViewModelProvider
import com.creatur.data.login.LoginRegisterRepository
import com.creatur.data.order.OrderRepository
import com.creatur.data.product.ProductRepository
import com.creatur.data.question.ResponseRepository
import com.creatur.data.venture.VentureRepository
import com.creatur.ui.buy.BuyProductViewModelFactory
import com.creatur.ui.home.HomeViewModelFactory
import com.creatur.ui.home.explora.CategoryActivity
import com.creatur.ui.home.explora.CategoryViewModel
import com.creatur.ui.home.explora.CategoryViewModelFactory
import com.creatur.ui.newProductFormActivity.NewProductViewModelFactory
import com.creatur.ui.newVentureFormActivity.NewVentureFormViewModelFactory
import com.creatur.ui.login.LoginViewModelFactory
import com.creatur.ui.orderStatus.OrderStatusViewModelFactory
import com.creatur.ui.register.RegisterViewModelFactory
import com.creatur.ui.response.ResponseViewModelFactory
import com.creatur.ui.ventureDetail.VentureDetail
import com.creatur.ui.ventureDetail.VentureViewModelFactory

/* Object used as an instance of the Factory of ViewModels for the Login*/
object InjectorUtils {

    fun provideLoginsViewModelFactory(): LoginViewModelFactory {
        val loginRepository = LoginRegisterRepository()
        return LoginViewModelFactory(
            loginRepository
        )
    }

    fun provideRegistersViewModelFactor(): RegisterViewModelFactory {
        val registerRepository = LoginRegisterRepository()
        return RegisterViewModelFactory(
            registerRepository
        )
    }

    fun provideResponseViewModelFactor(): ResponseViewModelFactory {
        val responseRepository = ResponseRepository()
        return ResponseViewModelFactory(
            responseRepository
        )
    }

    fun provideHomeViewModelFactory(application: Context): HomeViewModelFactory {
        val ventureRepository = VentureRepository()
        val productRepository = ProductRepository()
        return HomeViewModelFactory(
                application,
                ventureRepository,
                productRepository
        )
    }

    fun provideNewVentureViewModelFactory(application: Context): NewVentureFormViewModelFactory {
        val ventureRepository = VentureRepository()
        return NewVentureFormViewModelFactory(
            application,
            ventureRepository
        )
    }

    fun provideNewBuyProductViewModelFactory(): BuyProductViewModelFactory {
        val orderRepository = OrderRepository()
        return BuyProductViewModelFactory(
            orderRepository
        )
    }

    fun provideNewProductViewModelFactory(): NewProductViewModelFactory {
        val productRepository = ProductRepository()
        return NewProductViewModelFactory(
            productRepository
        )
    }

    fun provideOrderStatusViewModelFactory(): OrderStatusViewModelFactory {
        val orderRepository = OrderRepository()
        return OrderStatusViewModelFactory(
            orderRepository
        )
    }

    fun provideVentureViewModelFactory(ventureDetail: VentureDetail): VentureViewModelFactory {
        val productRepository = ProductRepository()
        return VentureViewModelFactory(
            productRepository
        )

    }

    fun provideCategoryViewModelFactory(categoryActivity: CategoryActivity): CategoryViewModelFactory {
        val productRepository = ProductRepository()
        val ventureRepository = VentureRepository()
        return CategoryViewModelFactory(
                productRepository,
                ventureRepository
        )
    }
}