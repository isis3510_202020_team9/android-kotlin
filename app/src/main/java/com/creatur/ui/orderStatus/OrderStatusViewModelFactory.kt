package com.creatur.ui.orderStatus

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.creatur.data.order.OrderRepository
import com.creatur.ui.home.HomeViewModel

class OrderStatusViewModelFactory(private val orderRepository: OrderRepository): ViewModelProvider.NewInstanceFactory() {
    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return OrderStatusViewModel(
                orderRepository
        ) as T
    }
}