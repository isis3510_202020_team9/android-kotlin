package com.creatur.ui.orderStatus

import android.graphics.Typeface
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.core.view.marginBottom
import androidx.core.view.marginLeft
import androidx.lifecycle.ViewModelProvider
import com.bumptech.glide.Glide
import com.creatur.R
import com.creatur.data.order.OrderStatus
import com.creatur.data.order.Review
import com.creatur.data.product.Product
import com.creatur.ui.login.TokenPref
import com.creatur.utilities.CheckConnectivity
import com.creatur.utilities.InjectorUtils
import kotlinx.android.synthetic.main.activity_order_status.*
import kotlinx.android.synthetic.main.activity_venture_detail.*
import java.lang.Exception

class OrderStatusActivity : AppCompatActivity() {
    private var ORDEN_DE_COMPRA = 0
    private var ORDEN_DE_VENTURE = 1
    private var ORDEN_DE_ACTIVIDAD = 2
    private var STATUSLIST = listOf<String>("NOT_STARTED", "IN_PROGRESS", "ON_WAY", "FINISHED", "DECLINED")
    private var producto: Product? = null
    private var reviewIndex: Int = -1
    private lateinit var status: OrderStatus
    private lateinit var viewModel: OrderStatusViewModel
    private var type: Int? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        type = intent.getIntExtra("type", ORDEN_DE_COMPRA)
        status = intent.getParcelableExtra("productOrderStatus")
        producto = if(type == ORDEN_DE_VENTURE || type == ORDEN_DE_ACTIVIDAD) {
            status.products?.get(0)?.product
        }
        else {
            intent.getParcelableExtra<Product>("productOrder")
        }
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_order_status)

        viewModel = ViewModelProvider(this, InjectorUtils.provideOrderStatusViewModelFactory()).get(OrderStatusViewModel::class.java)

        setStatus()

        fill()

        viewModel.updatedOrder.observe(this, {
            result -> result.apply {
                if(result != "none"){
                    if(result == "error"){
                        Toast.makeText(this@OrderStatusActivity, "Ocurrió un error al actualizar el estado", Toast.LENGTH_LONG).show()
                    } else {
                        status.status = result
                        setStatus()
                    }
                }
        }
        })

        viewModel.reviewOrder.observe(this, {
            result -> result.apply {
                if(result != "none"){
                    if(result == "error"){
                        Toast.makeText(this@OrderStatusActivity, "Ocurrió un error al calificar", Toast.LENGTH_LONG).show()
                    } else {
                        status.review = Review(reviewIndex, status.id)
                        defineButtons()
                    }
                }
        }
        })
    }

    fun fill(){
        if(producto != null) {
            Glide.with(this).load(producto!!.photo).centerCrop().fallback(R.drawable.ic_menu_camera).into(productImage)
            nombreproducto.text = producto!!.name
            costoproducto.text = "$" + producto!!.prize.toString()
        }

        Glide.with(this).load(status.venture?.photo).centerCrop().fallback(R.drawable.ic_menu_camera).into(ventureImage)
        nombreventure.text = status.venture?.name
        if(type == ORDEN_DE_VENTURE){
            fechacompra.text = status.createdAt.split("T")[0].replace("-","/")
        }
        else {
            fechacompra.text = status.createdAt
        }
        preciototal.text = "$" + status.finalPrize.toString()
    }

    fun setStatus(){
        val scale = applicationContext.resources.displayMetrics.density
        var index = -1
        for (i in STATUSLIST.indices){
            if(STATUSLIST[i] == status.status){
                index = i
                break
            }
        }

        if(index >= 1){
            icono3.layoutParams.width = (scale*30 + 0.5f).toInt()
            icono3.layoutParams.height = (scale*30 + 0.5f).toInt()
            estado3.setTypeface(estado3.typeface, Typeface.BOLD)
            if(status.status == "DECLINED"){
                icono3.setBackgroundResource(R.drawable.ic_statrejected)
                estado3.text = "Rechazada"
                defineButtons()
                return
            }
            else{
                icono3.setBackgroundResource(R.drawable.ic_statcompleted)
            }
        }

        if(index >= 2){
            icono2.layoutParams.width = (scale*30 + 0.5f).toInt()
            icono2.layoutParams.height = (scale*30 + 0.5f).toInt()
            estado2.setTypeface(estado2.typeface, Typeface.BOLD)
            icono2.setBackgroundResource(R.drawable.ic_statcompleted)
        }

        if(index >= 3){
            icono1.layoutParams.width = (scale*30 + 0.5f).toInt()
            icono1.layoutParams.height = (scale*30 + 0.5f).toInt()
            estado1.setTypeface(estado1.typeface, Typeface.BOLD)
            icono1.setBackgroundResource(R.drawable.ic_statcompleted)
        }

        defineButtons()
    }

    fun defineButtons(){
        acceptreject.visibility = View.GONE
        advance.visibility = View.GONE
        review.visibility = View.GONE
        spinner.visibility = View.GONE
        buttonreview.visibility = View.GONE
        if(type == ORDEN_DE_VENTURE){
            if(status.status == "NOT_STARTED"){
                acceptreject.visibility = View.VISIBLE
                advance1.setOnClickListener {
                    if(CheckConnectivity.checkConnectivity(this)){
                        viewModel.updateStatus(TokenPref(this).getToken()?:"nan", status.id, "IN_PROGRESS")
                    } else {
                        Toast.makeText(this@OrderStatusActivity, "No hay conexión a internet", Toast.LENGTH_LONG).show()
                    }
                }
                advance2.setOnClickListener {
                    if(CheckConnectivity.checkConnectivity(this)){
                        viewModel.updateStatus(TokenPref(this).getToken()?:"nan", status.id, "DECLINED")
                    } else {
                        Toast.makeText(this@OrderStatusActivity, "No hay conexión a internet", Toast.LENGTH_LONG).show()
                    }
                }
            }
            if(status.status == "IN_PROGRESS"){
                advance.visibility = View.VISIBLE
                advance.setOnClickListener {
                    if(CheckConnectivity.checkConnectivity(this)){
                        viewModel.updateStatus(TokenPref(this).getToken()?:"nan", status.id, "ON_WAY")
                    } else {
                        Toast.makeText(this@OrderStatusActivity, "No hay conexión a internet", Toast.LENGTH_LONG).show()
                    }
                }
            }
            if(status.status == "ON_WAY"){
                advance.visibility = View.VISIBLE
                advance.setOnClickListener {
                    if(CheckConnectivity.checkConnectivity(this)){
                        viewModel.updateStatus(TokenPref(this).getToken()?:"nan", status.id, "FINISHED")
                    } else {
                        Toast.makeText(this@OrderStatusActivity, "No hay conexión a internet", Toast.LENGTH_LONG).show()
                    }
                }
            }
        }
        if(type == ORDEN_DE_ACTIVIDAD && status.status == "FINISHED") {
            review.visibility = View.VISIBLE
            if(status.review == null){
                spinner.visibility = View.VISIBLE
                buttonreview.visibility = View.VISIBLE
                defineSpinnerAndButton()
            }
            else {
                reviewtext.text = "Calificaste con ${status.review!!.grade}"
            }
        }
    }

    fun defineSpinnerAndButton(){
        val arr = resources.getStringArray(R.array.review)
        val ad = ArrayAdapter(this, android.R.layout.simple_spinner_item, arr)
        spinner.adapter = ad
        spinner.onItemSelectedListener = object :
        AdapterView.OnItemSelectedListener {
            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                reviewIndex = p2+1
            }

            override fun onNothingSelected(p0: AdapterView<*>?) {
                reviewIndex = 1
            }
        }

        buttonreview.setOnClickListener {
            val review = Review(reviewIndex, status.id)
            Log.d("review", "$review")
            if(CheckConnectivity.checkConnectivity(this)){
                viewModel.reviewOrder(TokenPref(this).getToken()?:"nan", review)
            } else {
                Toast.makeText(this@OrderStatusActivity, "No hay conexión a internet", Toast.LENGTH_LONG).show()
            }
        }
    }
}