package com.creatur.ui.orderStatus

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.creatur.data.order.OrderRepository
import com.creatur.data.order.OrderStatus
import com.creatur.data.order.Review
import com.creatur.data.order.StatusUpdate
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class OrderStatusViewModel(private val orderRepository: OrderRepository): ViewModel() {

    val updatedOrder = MutableLiveData<String>()
    val reviewOrder = MutableLiveData<String>()

    init {
        updatedOrder.value = "none"
        reviewOrder.value = "none"
    }

    fun updateStatus(token: String, orderId: String, status: String) = viewModelScope.launch(Dispatchers.IO) {
        try{
            updatedOrder.postValue(orderRepository.updateStatus(token, orderId, StatusUpdate(status)).status)
        } catch (e: Exception){
            updatedOrder.postValue("error")
            Log.d("error", "${e.message}")
        }
    }

    fun reviewOrder(token: String, review: Review) = viewModelScope.launch(Dispatchers.IO) {
        try {
            orderRepository.review(token, review)
            reviewOrder.postValue("done")
        } catch (e: Exception){
            reviewOrder.postValue("error")
            Log.d("error", "${e.message}")
        }
    }

    fun reset(){
        updatedOrder.postValue("none")
    }

}