package com.creatur.ui.newVentureFormActivity

import android.content.Context
import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.creatur.data.venture.Venture
import com.creatur.data.venture.VentureRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class NewVentureFormViewModel(private val ventureRepository: VentureRepository): ViewModel() {

    val resp = MutableLiveData<String>()
    init {
        resp.value = "none"
    }

    fun start(token: String, venture: Venture) = viewModelScope.launch(Dispatchers.IO) {
        try {
            val vent = ventureRepository.startVenture(token, venture)
            resp.postValue("${vent.id}")
        } catch (e: Exception){

            resp.postValue("error")
        }
    }
}