package com.creatur.ui.newVentureFormActivity

import android.content.Context
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.creatur.data.venture.VentureRepository

class NewVentureFormViewModelFactory (private val application: Context, private val ventureRepository: VentureRepository): ViewModelProvider.NewInstanceFactory() {
    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return NewVentureFormViewModel(
            ventureRepository
        ) as T
    }
}