package com.creatur.ui.newVentureFormActivity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Spinner
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.creatur.R
import com.creatur.data.venture.Venture
import com.creatur.ui.login.TokenPref
import com.creatur.utilities.CheckConnectivity
import com.creatur.utilities.InjectorUtils
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.activity_new_venture_form.*

class NewVentureFormActivity : AppCompatActivity() {

    private lateinit var viewModel: NewVentureFormViewModel
    private lateinit var token: String

    private val cats = mutableListOf("CLOTHING","FAST_FOOD","PETS","TOYS","BAKERY","SPORTS")

    private var cat : Int = -1

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_new_venture_form)

        val cached = TokenPref(this).getVentCreate()

        Log.d("vals", "$cached")

        if (cached.isNotEmpty() && cached.elementAt(0) != "nan"){
            editnombre.setText(cached.elementAt(0))
            edittelefono.setText(cached.elementAt(1))
            editdireccion.setText(cached.elementAt(2))
            editfacebook.setText(cached.elementAt(3))
            editinstagram.setText(cached.elementAt(4))
            cat = cached.elementAt(5).toInt()
        }

        if(!CheckConnectivity.checkConnectivity(this)){
            displaySnackbar()
        }

        val preference = TokenPref(this)
        token = preference.getToken()?:"nan"

        viewModel = ViewModelProvider(this, InjectorUtils.provideNewVentureViewModelFactory(application))
            .get(NewVentureFormViewModel::class.java)

        initializeUi()
        defineSpinner()


        viewModel.resp.observe(this, Observer<String> { result ->
            result?.apply {
                if( result != "none"){
                    if(result != "error"){
                        TokenPref(this@NewVentureFormActivity).setRole("VENTURE")
                        TokenPref(this@NewVentureFormActivity).setVentId(result)
                        finish()
                    }
                    else {
                        Toast.makeText(this@NewVentureFormActivity, "La información es inválida, por favor revise lo ingresado.", Toast.LENGTH_LONG).show()
                    }
                }
            }
        })
    }

    private fun initializeUi() {
        button_login.setOnClickListener {
            val nom = editnombre.text.toString()
            val tel = edittelefono.text.toString()
            val dir = editdireccion.text.toString()
            val fac = editfacebook.text.toString()
            val ins = editinstagram.text.toString()
            if (nom.isEmpty() || nom.length>50){
                Toast.makeText(this, "El nombre no puede ser vacio ni ser mayor a 50 caracteres", Toast.LENGTH_LONG).show()
                return@setOnClickListener
            }
            if (tel.isEmpty() || tel.length>20){
                Toast.makeText(this, "El telefono no puede ser vacio ni tener mas de 20 números", Toast.LENGTH_LONG).show()
                return@setOnClickListener
            }
            if (dir.isEmpty()){
                Toast.makeText(this, "La dirección no puede ser vacia", Toast.LENGTH_LONG).show()
                return@setOnClickListener
            }
            if (fac.isEmpty()){
                Toast.makeText(this, "Debe tener una página de Facebook", Toast.LENGTH_LONG).show()
                return@setOnClickListener
            }
            if (ins.isEmpty()){
                Toast.makeText(this, "Debe tener una página de Instagram", Toast.LENGTH_LONG).show()
                return@setOnClickListener
            }
            val newVenture = Venture(
                name = nom,
                phone = tel,
                address = dir,
                facebookUrl = "https://www.facebook.com/$fac/",
                instagramUrl = "https://www.instagram.com/$ins/",
                categories = mutableListOf(cats[cat])
            )
            if(!CheckConnectivity.checkConnectivity(this)){
                displaySnackbar()
                TokenPref(this).setVentCreate(nom, tel, dir, fac, ins, cat.toString())
            }
            else{
                viewModel.start(token, newVenture)
                TokenPref(this).clearVentCreate()
            }
        }
    }

    private fun defineSpinner() {
        val spin : Spinner = findViewById(R.id.spin)
        val arr = resources.getStringArray(R.array.categorias)
        val ad = ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, arr)
        spin.adapter = ad
        if(cat != -1){
            spin.setSelection(cat)
        }
        spin.onItemSelectedListener = object :
            AdapterView.OnItemSelectedListener {
            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                cat = p2
            }

            override fun onNothingSelected(p0: AdapterView<*>?) {
                cat = 0
            }
        }
    }

    private fun displaySnackbar() {
        val snackBar = Snackbar.make(
            container, "No hay conexión a internet, tus datos se guardaran para la proxima vez que intentes registrarte",
            Snackbar.LENGTH_LONG
        )
        snackBar.show()
    }
}