package com.creatur.ui.newVentureFormActivity

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import com.creatur.R


/**
 * A simple [Fragment] subclass.
 * Use the [NewVentureFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class NewVentureFragment : Fragment() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_venture, container, false)
        val button = view.findViewById<Button>(R.id.buttonnewventure)
        button.setOnClickListener {
            val intent = Intent(activity, NewVentureFormActivity::class.java)
            startActivity(intent)
        }
        return view
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @return A new instance of fragment VentureFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance() =
            NewVentureFragment()
                .apply {
            }
    }
}