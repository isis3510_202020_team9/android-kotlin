package com.creatur.ui.register

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.creatur.data.login.LoginRegisterRepository

/* Class that represents a Factory of ViewModels, basically an abstract class that handles any LoginViewModel as generic */
class RegisterViewModelFactory(private val loginRegisterRepository: LoginRegisterRepository) : ViewModelProvider.NewInstanceFactory() {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return RegisterViewModel(
            loginRegisterRepository
        ) as T
    }
}