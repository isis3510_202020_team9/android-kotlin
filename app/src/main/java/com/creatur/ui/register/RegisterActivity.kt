package com.creatur.ui.register

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.creatur.R
import com.creatur.data.login.LoginRegister
import com.creatur.data.login.Token
import com.creatur.ui.login.TokenPref
import com.creatur.ui.response.ResponseActivity
import com.creatur.utilities.CheckConnectivity
import com.creatur.utilities.InjectorUtils
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.activity_register.*

class RegisterActivity : AppCompatActivity() {

    private lateinit var viewModel: RegisterViewModel

    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)
        supportActionBar?.hide()
        if(!CheckConnectivity.checkConnectivity(this)){
            displaySnackbar()
        }

        viewModel = ViewModelProvider(this, InjectorUtils.provideRegistersViewModelFactor())
            .get(RegisterViewModel::class.java)

        initializeUi()

        val preference = TokenPref(this)

        viewModel.token.observe(this, Observer<Token> { result ->
            result?.apply {
                if (result.token != "none"){
                    if( result.token == "Token invalido"){
                        Toast.makeText(this@RegisterActivity, "Ocurrió un error", Toast.LENGTH_LONG).show()
                    } else {
                        preference.setToken(result.token)
                        preference.setRole(result.role)
                        Toast.makeText(this@RegisterActivity, "Registro exitoso", Toast.LENGTH_LONG).show()
                        val intent = Intent(this@RegisterActivity, ResponseActivity::class.java)
                        startActivity(intent)
                        finish()
                        //TODO: Redirigir a Preguntas
                    }
                }
            }
        })
    }

    private fun initializeUi() {
        button_login.setOnClickListener {
            if(!CheckConnectivity.checkConnectivity(this)){
                displaySnackbar()
            }
            else {
                val em = editText_login.text.toString()
                val ps = editText_password.text.toString()
                val cn = editText_confirm.text.toString()
                if(ps != cn){
                    Toast.makeText(this, "Las contraseñas no coinciden", Toast.LENGTH_LONG).show()
                    return@setOnClickListener
                }
                if (em.length > 40 ) {
                    Toast.makeText(this, "El email es demasiado largo", Toast.LENGTH_LONG).show()
                    return@setOnClickListener
                }
                if (ps.length < 6 ) {
                    Toast.makeText(this, "La contraseña debe ser mayor a 6 caracteres", Toast.LENGTH_LONG).show()
                    return@setOnClickListener
                }
                if (ps.toLowerCase() == ps){
                    Toast.makeText(this, "La contraseña debe tener una mayuscula", Toast.LENGTH_LONG).show()
                    return@setOnClickListener
                }
                if (ps.toUpperCase() == ps){
                    Toast.makeText(this, "La contraseña debe tener una minuscula", Toast.LENGTH_LONG).show()
                    return@setOnClickListener
                }
                val register = LoginRegister(
                    em,
                    ps
                )
                viewModel.register(register)
            }
        }

        text_register.setOnClickListener {
            finish()
        }
    }

    private fun displaySnackbar() {
        val snackBar = Snackbar.make(
            container, "There is no internet connection",
            Snackbar.LENGTH_LONG
        )
        snackBar.show()
    }
}