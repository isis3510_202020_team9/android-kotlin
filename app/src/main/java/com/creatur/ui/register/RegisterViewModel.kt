package com.creatur.ui.register

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.creatur.data.login.LoginRegister
import com.creatur.data.login.LoginRegisterRepository
import com.creatur.data.login.Token
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.lang.Exception

/* Class that represents a View Model for the register*/
class RegisterViewModel(private val loginRegisterRepository: LoginRegisterRepository) :ViewModel() {

    val token = MutableLiveData<Token>()
    init {
        token.value = Token("none", "none")
    }

    fun register(loginRegister: LoginRegister) = viewModelScope.launch(Dispatchers.IO) {
        try {
            token.postValue(loginRegisterRepository.register(loginRegister))
        } catch (e: Exception){
            token.postValue(Token("Token invalido", "NaN"))
        }
    }
}