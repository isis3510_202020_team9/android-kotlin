package com.creatur.ui.login

import android.content.Context
import android.content.SharedPreferences

class TokenPref(context: Context) {
    val PREF_NAME = "TokenSharedPreference"
    val PREF_TOKEN = "token"
    val PREF_ROLE = "role"
    val PREF_VENTID = "ventid"
    val PREF_PRODCREATE = "productcreate"
    val PREF_VENTCREATE = "venturecreate"

    val preference: SharedPreferences = context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE)

    fun getToken(): String? {
        return preference.getString(PREF_TOKEN, "nan")
    }

    fun setToken(token:String){
        val editor = preference.edit()
        editor.putString(PREF_TOKEN, token)
        editor.apply()
    }

    fun getRole(): String? {
        return preference.getString(PREF_ROLE, "nan")
    }

    fun setRole(role:String) {
        val editor = preference.edit()
        editor.putString(PREF_ROLE, role)
        editor.apply()
    }

    fun getVentId(): String? {
        return preference.getString(PREF_VENTID, "nan")
    }

    fun setVentId(ventId:String) {
        val editor = preference.edit()
        editor.putString(PREF_VENTID, ventId)
        editor.apply()
    }

    fun getProdCreate(): List<String> {
        val lista = mutableListOf<String>()
        lista.add(preference.getString("${PREF_PRODCREATE}nam", "nan")?:"nan")
        lista.add(preference.getString("${PREF_PRODCREATE}pri", "0")?:"0")
        lista.add(preference.getString("${PREF_PRODCREATE}des", "nan")?:"nan")
        lista.add(preference.getString("${PREF_PRODCREATE}cat", "0")?:"0")
        return lista
    }

    fun clearProdCreate() {
        val editor = preference.edit()
        editor.remove("${PREF_PRODCREATE}nam")
        editor.remove("${PREF_PRODCREATE}pri")
        editor.remove("${PREF_PRODCREATE}des")
        editor.remove("${PREF_PRODCREATE}cat")
        editor.apply()
    }

    fun setProdCreate(name: String, price: String, desc: String, cat: String) {
        clearProdCreate()
        val editor = preference.edit()
        editor.putString("${PREF_PRODCREATE}nam", name)
        editor.putString("${PREF_PRODCREATE}pri", price)
        editor.putString("${PREF_PRODCREATE}des", desc)
        editor.putString("${PREF_PRODCREATE}cat", cat)
        editor.apply()
    }

    fun getVentCreate(): List<String> {
        val lista = mutableListOf<String>()
        lista.add(preference.getString("${PREF_VENTCREATE}nam", "nan")?:"nan")
        lista.add(preference.getString("${PREF_VENTCREATE}pho", "nan")?:"nan")
        lista.add(preference.getString("${PREF_VENTCREATE}add", "nan")?:"nan")
        lista.add(preference.getString("${PREF_VENTCREATE}fb", "nan")?:"nan")
        lista.add(preference.getString("${PREF_VENTCREATE}ins", "nan")?:"nan")
        lista.add(preference.getString("${PREF_VENTCREATE}cat", "0")?:"0")
        return lista
    }

    fun clearVentCreate() {
        val editor = preference.edit()
        editor.remove("${PREF_VENTCREATE}nam")
        editor.remove("${PREF_VENTCREATE}pho")
        editor.remove("${PREF_VENTCREATE}add")
        editor.remove("${PREF_VENTCREATE}fb")
        editor.remove("${PREF_VENTCREATE}ins")
        editor.remove("${PREF_VENTCREATE}cat")
        editor.apply()
    }

    fun setVentCreate(name: String, phone: String, addr: String, fb: String, ins: String, cat: String) {
        clearVentCreate()
        val editor = preference.edit()
        editor.putString("${PREF_VENTCREATE}nam", name)
        editor.putString("${PREF_VENTCREATE}pho", phone)
        editor.putString("${PREF_VENTCREATE}add", addr)
        editor.putString("${PREF_VENTCREATE}fb", fb)
        editor.putString("${PREF_VENTCREATE}ins", ins)
        editor.putString("${PREF_VENTCREATE}cat", cat)
        editor.apply()
    }
}