package com.creatur.ui.login

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.creatur.data.login.LoginRegister
import com.creatur.data.login.LoginRegisterRepository
import com.creatur.data.login.Token
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.lang.Exception

/* Class that represents a View Model for the login*/
class LoginViewModel(private val loginRegisterRepository: LoginRegisterRepository) :ViewModel() {

    val token = MutableLiveData<Token>()
    init {
        token.value = Token("none", "none")
    }

    fun login(loginRegister: LoginRegister) = viewModelScope.launch(Dispatchers.IO) {
        try {
            token.postValue(loginRegisterRepository.login(loginRegister))
        } catch (e: Exception){
            token.postValue(Token("Token invalido", "NaN"))
        }
    }
}