package com.creatur.ui.login

import com.creatur.ui.home.HomeActivity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.creatur.R
import com.creatur.data.login.LoginRegister
import com.creatur.data.login.Token
import com.creatur.ui.register.RegisterActivity
import com.creatur.utilities.CheckConnectivity
import com.creatur.utilities.InjectorUtils
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.activity_login.*


class LoginActivity : AppCompatActivity() {

    private lateinit var viewModel: LoginViewModel


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        val preference = TokenPref(this)
        var initialToken = preference.getToken()
        if (initialToken != "nan"){
            val intent = Intent(this@LoginActivity, HomeActivity::class.java)
            startActivity(intent)
        }

        supportActionBar?.hide()
        if(!CheckConnectivity.checkConnectivity(this)){
            displaySnackbar()
        }

        viewModel = ViewModelProvider(this, InjectorUtils.provideLoginsViewModelFactory())
            .get(LoginViewModel::class.java)
        initializeUi()


        viewModel.token.observe(this, Observer<Token> { result ->
            result?.apply {
                if( result.token != "none" ){
                    if( result.token == "Token invalido"){
                        Toast.makeText(this@LoginActivity, "La información ingresada no existe o no es válida", Toast.LENGTH_LONG).show()
                    } else {
                        preference.setToken(result.token)
                        preference.setRole(result.role)
                        Toast.makeText(this@LoginActivity, "Ingreso exitoso", Toast.LENGTH_LONG).show()
                        val intent = Intent(this@LoginActivity, HomeActivity::class.java)
                        startActivity(intent)
                    }
                }
            }
        })
    }

    private fun initializeUi() {
        button_login.setOnClickListener {
            if(!CheckConnectivity.checkConnectivity(this)){
                displaySnackbar()
            }
            else {
                val em = editText_login.text.toString()
                val ps = editText_password.text.toString()
                if (em.length > 40 ) {
                    Toast.makeText(this, "El email es demasiado largo", Toast.LENGTH_LONG).show()
                    return@setOnClickListener
                }
                if (ps.length < 8 ) {
                    Toast.makeText(this, "La contraseña debe ser mayor a 8 caracteres", Toast.LENGTH_LONG).show()
                    return@setOnClickListener
                }
                val login = LoginRegister(
                    em,
                    ps
                )
                viewModel.login(login)
            }
        }

        text_register.setOnClickListener {
            val intent = Intent(this, RegisterActivity::class.java)
            startActivity(intent)
        }
    }

    private fun displaySnackbar() {
        val snackBar = Snackbar.make(
            container, "No hay conexión a internet",
            Snackbar.LENGTH_LONG
        )
        snackBar.show()
    }
}

