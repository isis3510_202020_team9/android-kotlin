package com.creatur.ui.ventureDetail

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager.widget.ViewPager
import com.bumptech.glide.Glide
import com.creatur.R
import com.creatur.data.product.Product
import com.creatur.data.venture.Venture
import com.creatur.databinding.ActivityProductDetailBinding
import com.creatur.databinding.ActivityVentureDetailBinding
import com.creatur.ui.home.inicio.reccomended.ProductAdapterRecommended
import com.creatur.ui.login.TokenPref
import com.creatur.ui.productDetail.ProductDetail
import com.creatur.ui.productDetail.producto
import com.creatur.utilities.InjectorUtils
import com.google.android.material.appbar.CollapsingToolbarLayout
import com.google.android.material.internal.ContextUtils.getActivity

class VentureDetail() : AppCompatActivity(), ProductAdapterVenture.OnProductClickListener {
    var negocio: Venture? = null
    private lateinit var viewModel: VentureDetailViewModel
    var dataProducts: List <Product> = emptyList()

    override fun onCreate(savedInstanceState: Bundle?) {
        negocio = intent.getParcelableExtra<Venture>("negocio")
        super.onCreate(savedInstanceState)

        val preference = TokenPref(this)

        viewModel = this.run {
            ViewModelProvider(this, InjectorUtils.provideVentureViewModelFactory(this))
                    .get(VentureDetailViewModel::class.java)
        }

        val idNegocio = this.negocio!!.id
        val binding = ActivityVentureDetailBinding.inflate(layoutInflater)
        if (idNegocio != null) {
            viewModel.productDetail(preference.getToken()?:"nan", idNegocio)
            val layoutManagerProduct = LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
            val myListProducts = binding.ProductsRecyclerView
            viewModel.productslistdetail.observe(this, Observer<List<Product>>{
                    result -> result?.apply{
                dataProducts = result
                val adapterProducts = ProductAdapterVenture(dataProducts,this@VentureDetail)
                myListProducts.adapter = adapterProducts
            }
            })

            myListProducts.layoutManager = layoutManagerProduct

        }



        if (negocio != null) {
            binding.nombreVenture.text = negocio!!.name
            Glide.with(this).load(negocio!!.photo).centerCrop().fallback(R.drawable.ic_menu_camera)
                .into(binding.fotoVenture)
            binding.direccion.text = negocio!!.address
            binding.button.setOnClickListener {
                val intent = Intent(Intent.ACTION_DIAL,Uri.parse("tel:" + negocio!!.phone) )
                if (intent.resolveActivity(packageManager) != null) {
                    try {
                        // Launch the Phone app's dialer with a phone
                        // number to dial a call.
                        startActivity(intent)
                    } catch (s: SecurityException) {
                        // show() method display the toast with
                        // exception message.
                        Toast.makeText(this, "Error accediendo al Teléfono", Toast.LENGTH_LONG)
                            .show()
                    }

                } else {
                    Toast.makeText(this, "Teléfono Inválido", Toast.LENGTH_LONG)
                        .show()
                }
            }
            binding.button2.setOnClickListener{
                val intent = Intent(Intent.ACTION_VIEW, Uri.parse(negocio!!.facebookUrl))
                //TODO verificar internet
                if (intent.resolveActivity(packageManager) != null) {
                    startActivity(intent)
                }
                else {
                    Toast.makeText(this, "No se pudo encontrar una aplicación permitida para realizar la operación requerida.", Toast.LENGTH_LONG)
                        .show()
                }
            }

            binding.button3.setOnClickListener{
                val intent = Intent(Intent.ACTION_VIEW, Uri.parse(negocio!!.instagramUrl))
                //TODO verificar internet
                if (intent.resolveActivity(packageManager) != null) {
                    startActivity(intent)
                }
                else {
                    Toast.makeText(this, "No se pudo encontrar una aplicación permitida para realizar la operación requerida.", Toast.LENGTH_LONG)
                        .show()
                }
            }
            if(negocio!!.instagramUrl == null)
            {
                
            }
            setContentView(binding.root)
        }


    }

    override fun onProductClick(product: Product) {
        //llamar a una función en activity
        val showProductDetail = Intent (this, ProductDetail::class.java)
        showProductDetail.putExtra("producto", product)
        startActivity(showProductDetail)
    }
}