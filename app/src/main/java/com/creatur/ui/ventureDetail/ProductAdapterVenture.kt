package com.creatur.ui.ventureDetail

import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.lifecycle.LifecycleOwner
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.creatur.R
import com.creatur.data.product.Product
import com.creatur.ui.productDetail.ProductDetail
import com.google.android.material.internal.ContextUtils.getActivity

class ProductAdapterVenture(val lista: List<Product>, private val listener: ProductAdapterVenture.OnProductClickListener) : RecyclerView.Adapter<ProductAdapterVenture.ViewHolder>(){
    inner class ViewHolder(val view: View):RecyclerView.ViewHolder(view) {
        init {
            view.setOnClickListener {
                val position: Int = adapterPosition
                if (position != RecyclerView.NO_POSITION) {
                    val product = lista[position]
                    listener.onProductClick(product)
                }
            }
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.product_item, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount() = lista.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val name = holder.view.findViewById<TextView>(R.id.textView_product)
        name.text = lista[position].name
        val price = holder.view.findViewById<TextView>(R.id.textView_price)
        price.text = lista[position].prize.toString()
        val url = lista[position].photo
        val photoItem = holder.view.findViewById<ImageView>(R.id.image_product)
        Glide.with(photoItem.context).load(url).centerCrop().fallback(R.drawable.ic_menu_camera).into(photoItem)
    }
    interface OnProductClickListener{
        fun onProductClick(product:Product)
    }
}