package com.creatur.ui.ventureDetail

import android.content.Context
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.creatur.data.product.ProductRepository

/* Class that represents a Factory of ViewModels, basically an abstract class that handles any HomeViewModel as generic */
class VentureViewModelFactory( private val productRepository: ProductRepository): ViewModelProvider.NewInstanceFactory() {
    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return VentureDetailViewModel(
            productRepository
        ) as T
    }
}