package com.creatur.ui.ventureDetail

import android.content.Context
import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.creatur.data.DatabaseLocal
import com.creatur.data.product.Product
import com.creatur.data.product.ProductRepository
import com.creatur.data.venture.VentureRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class VentureDetailViewModel(private val productRepository: ProductRepository) : ViewModel() {
    val productslistdetail = MutableLiveData<List<Product>>()
    init {
        productslistdetail.value = null
    }

    fun productDetail(token:String,  ventureId: String ) = viewModelScope.launch(Dispatchers.IO) {
        try {
            productslistdetail.postValue(productRepository.productDetail(token, ventureId))
        }
        catch (e: Exception){
            Log.d("Error", e.message)
        }

    }
}