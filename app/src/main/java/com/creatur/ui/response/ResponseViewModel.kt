package com.creatur.ui.response

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.creatur.data.question.Response
import com.creatur.data.question.ResponseRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

/* Class that represents a View Model for the profilling responses of the user*/
class ResponseViewModel(private val responseRepository: ResponseRepository): ViewModel() {
    val message = MutableLiveData<String>()
    init {
        message.value = "none"
    }

    fun profilling(token: String, response: Response) = viewModelScope.launch(Dispatchers.IO){
        message.postValue(responseRepository.profiling(token, response).message)
    }
}