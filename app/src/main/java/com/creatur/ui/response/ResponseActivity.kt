package com.creatur.ui.response

import com.creatur.ui.home.HomeActivity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Spinner
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.creatur.R
import com.creatur.data.question.Question
import com.creatur.data.question.Response
import com.creatur.ui.login.TokenPref
import com.creatur.utilities.CheckConnectivity
import com.creatur.utilities.InjectorUtils
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.activity_response.*

class ResponseActivity : AppCompatActivity() {

    private lateinit var viewModel: ResponseViewModel

    private var ques : List<Question> = mutableListOf(Question(1,1), Question(2,1), Question(3,1))

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_response)
        supportActionBar?.hide()
        if(!CheckConnectivity.checkConnectivity(this)){
            displaySnackbar()
        }

        viewModel = ViewModelProvider(this, InjectorUtils.provideResponseViewModelFactor())
            .get(ResponseViewModel::class.java)

        val preference = TokenPref(this)

        defineSpinners()
        defineButton(preference.getToken()?:"nan")

        viewModel.message.observe(this, Observer<String> { result ->
            result?.apply {
                if( result != "none"){
                    Log.d("Resultado", "${result}")
                    val intent = Intent(this@ResponseActivity, HomeActivity::class.java)
                    startActivity(intent)
                    finish()
                }
            }
        })
    }

    private fun defineButton(token: String) {
        button_send.setOnClickListener {
            if(!CheckConnectivity.checkConnectivity(this)){
                displaySnackbar()
            }
            else {
                if(token == "nan") {
                    Toast.makeText(this, "Registro invalido", Toast.LENGTH_LONG).show()
                    return@setOnClickListener
                }
                var response = Response(ques)
                viewModel.profilling(token, response)
            }
        }
    }

    private fun defineSpinners() {
        val spin1 : Spinner = findViewById(R.id.spin1)
        val spin2 : Spinner = findViewById(R.id.spin2)
        val spin3 : Spinner = findViewById(R.id.spin3)

        val arr1 = resources.getStringArray(R.array.res1)
        val ad1 = ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, arr1)
        spin1.adapter = ad1
        spin1.onItemSelectedListener = object :
        AdapterView.OnItemSelectedListener {
            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                ques[0].answer = p2+1
            }

            override fun onNothingSelected(p0: AdapterView<*>?) {
                ques[0].answer = 1
            }
        }

        val arr2 = resources.getStringArray(R.array.res2)
        val ad2 = ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, arr2)
        spin2.adapter = ad2
        spin2.onItemSelectedListener = object :
            AdapterView.OnItemSelectedListener {
            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                ques[1].answer = p2+1
            }

            override fun onNothingSelected(p0: AdapterView<*>?) {
                ques[1].answer = 1
            }
        }

        val arr3 = resources.getStringArray(R.array.res3)
        val ad3 = ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, arr3)
        spin3.adapter = ad3
        spin3.onItemSelectedListener = object :
            AdapterView.OnItemSelectedListener {
            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                ques[2].answer = p2+1
            }

            override fun onNothingSelected(p0: AdapterView<*>?) {
                ques[2].answer = 1
            }
        }
    }

    private fun displaySnackbar() {
        val snackBar = Snackbar.make(
            container, "There is no internet connection",
            Snackbar.LENGTH_LONG
        )
        snackBar.show()
    }
}