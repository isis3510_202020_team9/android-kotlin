package com.creatur.ui.response

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.creatur.data.question.ResponseRepository

class ResponseViewModelFactory(private val responseRepository: ResponseRepository): ViewModelProvider.NewInstanceFactory() {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return ResponseViewModel(
            responseRepository
        ) as T
    }
}