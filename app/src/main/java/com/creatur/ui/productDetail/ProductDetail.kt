package com.creatur.ui.productDetail

import android.content.Intent
import android.os.Bundle
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil.setContentView
import androidx.fragment.app.Fragment
import com.bumptech.glide.Glide
import com.creatur.R
import com.creatur.data.product.Product
import com.creatur.databinding.ActivityProductDetailBinding
import com.creatur.ui.buy.BuyProductActivity
import com.creatur.ui.home.inicio.reccomended.ProductAdapterRecommended
import com.creatur.ui.login.TokenPref
import com.creatur.utilities.CheckConnectivity
import com.google.android.material.snackbar.Snackbar

var producto: Product? = null
class ProductDetail : AppCompatActivity(R.layout.activity_product_detail) {

    private lateinit var binding: ActivityProductDetailBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        producto = intent.getParcelableExtra("producto")
        super.onCreate(savedInstanceState)
        binding = ActivityProductDetailBinding.inflate(layoutInflater)
        if(producto!=null){
            binding.nombreProducto.text = producto!!.name
            Glide.with(this).load(producto!!.photo).centerCrop().fallback(R.drawable.ic_menu_camera).into(binding.fotoProducto)
            binding.precioProducto.text = "$ " + producto!!.prize.toString()
            binding.productDescription.text = producto!!.description
            binding.buttonComprar.setOnClickListener{
                if(!CheckConnectivity.checkConnectivity(this)){
                    displaySnackbar()
                }
                else{
                    val intent = Intent(this, BuyProductActivity::class.java)
                    intent.putExtra("productoBuy", producto)
                    startActivity(intent)
                }
            }
            setContentView(binding.root)
        }


    }

    private fun displaySnackbar() {
        val snackBar = Snackbar.make(
                binding.container, "No hay conexión a internet, tus datos se guardaran para la proxima vez que intentes registrarte",
                Snackbar.LENGTH_LONG
        )
        snackBar.show()
    }

}