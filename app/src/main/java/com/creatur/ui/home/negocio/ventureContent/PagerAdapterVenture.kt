package com.creatur.ui.home.negocio.ventureContent

import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.creatur.ui.home.inicio.reccomended.RecommendedHome
import com.creatur.ui.home.negocio.ordenes.VentureOrders
import com.creatur.ui.home.negocio.productos.VentureProducts

class  PagerAdapterVenture ( fm :  FragmentManager ): FragmentPagerAdapter(fm)  {

    override  fun  getItem ( position :  Int ): Fragment {
        return  when (position) {
            0  -> {
                VentureOrders ()
            }
            1  -> {
                VentureProducts()
            }
            else  -> {
                return  VentureOrders ()
            }
        }
    }


    override fun getCount(): Int {
        return 2
    }
    override  fun  getPageTitle ( position :  Int ): CharSequence {
        return  when (position) {
            0  ->  {" Ordenes "}
            else ->  {return " Productos "}

        }
    }
}
