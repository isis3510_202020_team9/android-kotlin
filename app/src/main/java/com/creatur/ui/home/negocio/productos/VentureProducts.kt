package com.creatur.ui.home.negocio.productos

import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.creatur.R
import com.creatur.data.product.Product
import com.creatur.data.venture.Venture
import com.creatur.ui.home.HomeViewModel
import com.creatur.ui.login.TokenPref
import com.creatur.ui.newProductFormActivity.NewProductFormActivity
import com.creatur.utilities.InjectorUtils
import com.google.android.material.floatingactionbutton.FloatingActionButton


/**
 * A fragment representing a list of Items.
 */
class VentureProducts : Fragment() {

    private lateinit var viewModel: HomeViewModel

    private var columnCount = 1

    private var REQUEST_OK = 200
    private var REQUEST_CODE = 190

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        arguments?.let {
            columnCount = it.getInt(ARG_COLUMN_COUNT)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_venture_products_list, container, false)
        val recycler = view.findViewById<RecyclerView>(R.id.list)

        viewModel = activity?.run {
            ViewModelProvider(this, InjectorUtils.provideHomeViewModelFactory(view.context))
                    .get(HomeViewModel::class.java)
        } ?: throw Exception("Error con activity principal de Home")

        val button = view.findViewById<FloatingActionButton>(R.id.floating_action_button)
        button.setOnClickListener {
            var intent = Intent(activity, NewProductFormActivity::class.java)
            startActivityForResult(intent, REQUEST_CODE)
        }

        val prefs = TokenPref(this.context?:throw Exception("Error de context"))

        viewModel.myvent(prefs.getToken()?:"nan")
        viewModel.myvent.observe(viewLifecycleOwner, {
            result -> result.apply {
            if(result != null){
                prefs.setVentId(result.id?:"nan")
                viewModel.listVentureProducts(prefs.getToken()?:"nan", prefs.getVentId()?:"nan")
            }
        }
        })

        // Set the adapter
        viewModel.ventureProducts.observe(viewLifecycleOwner, Observer<List<Product>>{
            result -> result.apply {
            if (recycler is RecyclerView) {
                with(recycler) {
                    layoutManager = when {
                        columnCount <= 1 -> LinearLayoutManager(context)
                        else -> GridLayoutManager(context, columnCount)
                    }

                    adapter = VentureProductsRecyclerViewAdapter(result)
                }
            }
        }
        })
        return view
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        Log.d("aca", "llego")
        if(requestCode == REQUEST_CODE && resultCode == REQUEST_OK){
//            val prod = data?.getParcelableExtra<Product>("product")
            val prefs = TokenPref(this.context?:throw Exception("Error de context"))
            viewModel.listVentureProducts(prefs.getToken()?:"nan", prefs.getVentId()?:"nan")
        }
    }

    companion object {

        // TODO: Customize parameter argument names
        const val ARG_COLUMN_COUNT = "column-count"

        // TODO: Customize parameter initialization
        @JvmStatic
        fun newInstance(columnCount: Int) =
            VentureProducts().apply {
                arguments = Bundle().apply {
                    putInt(ARG_COLUMN_COUNT, columnCount)
                }
            }
    }
}