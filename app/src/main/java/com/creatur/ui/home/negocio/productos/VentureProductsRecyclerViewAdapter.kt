package com.creatur.ui.home.negocio.productos

import android.content.Intent
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.FragmentActivity
import com.bumptech.glide.Glide
import com.creatur.R
import com.creatur.data.product.Product
import com.creatur.ui.newProductFormActivity.NewProductFormActivity


/**
 * [RecyclerView.Adapter] that can display a [Product].
 * TODO: Replace the implementation with code for your data type.
 */
class VentureProductsRecyclerViewAdapter(
        private val lista: List<Product>
) : RecyclerView.Adapter<VentureProductsRecyclerViewAdapter.ViewHolder>() {

    inner class ViewHolder(val view: View):RecyclerView.ViewHolder(view) {
        init {

        }

    }

    override fun getItemViewType(position: Int): Int {
        return 0;
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.fragment_venture_products, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val name = holder.view.findViewById<TextView>(R.id.textView_product)
        name.text = lista[position].name
        val price = holder.view.findViewById<TextView>(R.id.textView_price)
        price.text = lista[position].prize.toString()
        val url = lista[position].photo
        val photoItem = holder.view.findViewById<ImageView>(R.id.image_product)
        Glide.with(photoItem.context).load(url).centerCrop().fallback(R.drawable.ic_menu_camera).into(photoItem)

    }


    override fun getItemCount(): Int = lista.size


}