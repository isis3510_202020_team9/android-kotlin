package com.creatur.ui.home.actividad

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.creatur.R
import com.creatur.data.order.OrderStatus

class ActivityOrdersRecyclerViewAdapter (
    private val values: List<OrderStatus>,
    private val listener: OnOrderClickListener
) : RecyclerView.Adapter<ActivityOrdersRecyclerViewAdapter.ViewHolder>() {

    private var STATUSLIST = listOf<String>("NOT_STARTED", "IN_PROGRESS", "ON_WAY", "FINISHED", "DECLINED")

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.fragment_venture_orders, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = values[position]
        holder.name.text = item.products?.get(0)?.product?.name?:"Titulo"
        when (item.status) {
            STATUSLIST[0] -> {
                holder.state.text ="Recibida"
            }
            STATUSLIST[1] -> {
                holder.state.text ="Aceptada"
            }
            STATUSLIST[2] -> {
                holder.state.text ="En Camino"
            }
            STATUSLIST[3] -> {
                holder.state.text ="Completada"
            }
            STATUSLIST[4] -> {
                holder.state.text ="Rechazada"
            }
        }

        holder.date.text = item.createdAt.split("T")[0].replace("-","/")
        holder.cost.text = "$"+item.finalPrize.toString()
        Glide.with(holder.photoView.context).load(item.venture?.photo).centerCrop().fallback(R.drawable.ic_menu_camera).into(holder.photoView)
    }

    override fun getItemCount(): Int = values.size

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view){
        val name: TextView = view.findViewById(R.id.productName)
        val state: TextView = view.findViewById(R.id.orderState)
        val date: TextView = view.findViewById(R.id.orderDate)
        val cost: TextView = view.findViewById(R.id.orderCost)
        val photoView: ImageView = view.findViewById(R.id.orderImage)

        init {
            view.setOnClickListener {
                val position: Int = adapterPosition
                if (position != RecyclerView.NO_POSITION) {
                    val product = values[position]
                    listener.onOrderClick(product)
                }
            }
        }

        override fun toString(): String {
            return super.toString() + " '" + name.text + "'"
        }
    }

    interface OnOrderClickListener{
        fun onOrderClick(order: OrderStatus)
    }


}