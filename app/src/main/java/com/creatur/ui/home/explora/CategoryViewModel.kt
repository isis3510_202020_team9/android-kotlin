package com.creatur.ui.home.explora

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.creatur.data.product.Product
import com.creatur.data.product.ProductRepository
import com.creatur.data.venture.Venture
import com.creatur.data.venture.VentureRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class CategoryViewModel(private val productRepository: ProductRepository, private val ventureRepository: VentureRepository): ViewModel()  {
    val productslistcategory = MutableLiveData<List<Product>>()
    val ventureslistcategory = MutableLiveData<List<Venture>>()
    init {
        productslistcategory.value = null
        ventureslistcategory.value = null
    }

    fun productCategory(token:String,  category: String ) = viewModelScope.launch(Dispatchers.IO) {
        try {
            productslistcategory.postValue(productRepository.productCategory(token, category))
        }
        catch (e: Exception){

        }

    }

    fun ventureCategory(token:String,  category: String ) = viewModelScope.launch(Dispatchers.IO) {
        try {
            ventureslistcategory.postValue(ventureRepository.ventureCategory(token, category))
        }
        catch (e: Exception){

        }

    }
}