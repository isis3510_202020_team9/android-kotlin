package com.creatur.ui.home.negocio.ordenes

import android.content.Intent
import android.graphics.Rect
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.creatur.R
import com.creatur.data.order.Order
import com.creatur.data.order.OrderStatus
import com.creatur.data.product.Product
import com.creatur.data.venture.Venture
import com.creatur.ui.home.HomeViewModel
import com.creatur.ui.home.negocio.ordenes.dummy.DummyContent
import com.creatur.ui.login.TokenPref
import com.creatur.ui.orderStatus.OrderStatusActivity
import com.creatur.ui.productDetail.ProductDetail
import com.creatur.utilities.CheckConnectivity
import com.creatur.utilities.InjectorUtils

/**
 * A fragment representing a list of Items.
 */
class VentureOrders : Fragment(), VentureOrdersRecyclerViewAdapter.OnOrderClickListener {

    private lateinit var viewModel: HomeViewModel
    private var columnCount = 1
    private lateinit var venture: Venture

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        arguments?.let {
            columnCount = it.getInt(ARG_COLUMN_COUNT)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_venture_orders_list, container, false)

        viewModel = activity?.run {
            ViewModelProvider(this, InjectorUtils.provideHomeViewModelFactory(view.context))
                    .get(HomeViewModel::class.java)
        } ?: throw Exception("Error con activity principal de Home")

        val prefs = TokenPref(this.context?:throw Exception("Error de context"))

        if(!CheckConnectivity.checkConnectivity(activity?:throw Exception("Error con activity principal de Home"))){
            Toast.makeText(activity, "No hay conexión a internet", Toast.LENGTH_LONG).show()
        }

        viewModel.myvent(prefs.getToken()?:"nan")
        viewModel.myvent.observe(viewLifecycleOwner, {
            result -> result.apply {
            if(result != null){
                prefs.setVentId(result.id?:"nan")
                venture = result
                viewModel.listVentureOrders(prefs.getToken()?:"nan")
            }
        }
        })

        viewModel.ventureOrders.observe(viewLifecycleOwner, Observer<List<OrderStatus>>{
            result -> result.apply {
            if (view is RecyclerView) {
                view.addItemDecoration(MarginItemDecoration(2))
                with(view) {
                    layoutManager = when {
                        columnCount <= 1 -> LinearLayoutManager(context)
                        else -> GridLayoutManager(context, columnCount)
                    }
                    adapter = VentureOrdersRecyclerViewAdapter(result, this@VentureOrders)
                }
            }
        }
        })

        // Set the adapter

        return view
    }

    companion object {

        // TODO: Customize parameter argument names
        const val ARG_COLUMN_COUNT = "column-count"

        // TODO: Customize parameter initialization
        @JvmStatic
        fun newInstance(columnCount: Int) =
            VentureOrders().apply {
                arguments = Bundle().apply {
                    putInt(ARG_COLUMN_COUNT, columnCount)
                }
            }
    }

    override fun onOrderClick(order: OrderStatus) {
        val intent = Intent (activity, OrderStatusActivity::class.java)
        order.venture = venture
        intent.putExtra("productOrderStatus", order)
        intent.putExtra("type", 1)
        startActivity(intent)
    }
}

class MarginItemDecoration(private val spaceHeight: Int) : RecyclerView.ItemDecoration() {
    override fun getItemOffsets(outRect: Rect, view: View,
                                parent: RecyclerView, state: RecyclerView.State) {
        with(outRect) {
            if (parent.getChildAdapterPosition(view) == 0) {
                top = spaceHeight
            }
            left =  spaceHeight
            right = spaceHeight
            bottom = spaceHeight
        }
    }
}