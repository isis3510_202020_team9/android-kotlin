package com.creatur.ui.home.inicio
import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.creatur.ui.home.explora.SearchFragment
import com.creatur.ui.home.inicio.reccomended.RecommendedHome


class  MyPagerAdapter ( fm :  FragmentManager ): FragmentPagerAdapter(fm)  {

    override  fun  getItem ( position :  Int ): Fragment {
        return  when (position) {
            0  -> {
                RecommendedHome ()
            }
            1  -> {
                RecommendedHome ()
            }
            2  -> {
                RecommendedHome ()
            }
            else  -> {
                return  RecommendedHome ()
            }
        }
    }


    override fun getCount(): Int {
        return 3
    }
    override  fun  getPageTitle ( position :  Int ): CharSequence {
        return  when (position) {
            0  ->  {" Recomendados "}
            1   ->  {" Cercanos "}
            else ->  {return " Tendencias "}

        }
    }
}