package com.creatur.ui.home

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.net.ConnectivityManager
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import androidx.lifecycle.ViewModel
import com.creatur.R
import com.creatur.ui.home.actividad.ActivityFragment
import com.creatur.ui.home.explora.SearchFragment
import com.creatur.ui.home.inicio.HomeFragment
import com.creatur.ui.newVentureFormActivity.NewVentureFragment
import com.creatur.ui.home.negocio.ventureContent.UserVentureFragment
import com.creatur.ui.login.TokenPref
import com.creatur.utilities.CheckConnectivity
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.home_activity.*


class HomeActivity : AppCompatActivity() {

    private var negocio: Boolean = false
    private lateinit var toolbar: Toolbar

    private val mOnNavigationItemSelectedListener: BottomNavigationView.OnNavigationItemSelectedListener =
        object : BottomNavigationView.OnNavigationItemSelectedListener {
            override fun onNavigationItemSelected(item: MenuItem): Boolean {
                negocio = false
                when (item.itemId) {
                    R.id.navigation_home -> {
                        replaceFragment(HomeFragment.newInstance(), FRAGMENT_HOME)
                        return true
                    }
                    R.id.navigation_actividad -> {
                        replaceFragment(ActivityFragment.newInstance(), FRAGMENT_ACTIVIDAD)
                        return true
                    }
                    R.id.navigation_explora -> {
                        replaceFragment(SearchFragment.newInstance(), FRAGMENT_EXPLORA)
                        return true
                    }
                    R.id.navigation_negocio -> {
                        negocio = true
                        if(TokenPref(this@HomeActivity).getRole() == "CLIENT"){
                            replaceFragment(NewVentureFragment.newInstance(), FRAGMENT_NEGOCIO)
                        } else {
                            replaceFragment(UserVentureFragment.newInstance(), FRAGMENT_NEGOCIO)
                        }
                        return true
                    }
                }
                return false
            }
        }

    override fun onResume() {
        super.onResume()
        if (negocio){
            if(TokenPref(this@HomeActivity).getRole() == "CLIENT"){
                replaceFragment(NewVentureFragment.newInstance(), FRAGMENT_NEGOCIO)
            } else {
                replaceFragment(UserVentureFragment.newInstance(), FRAGMENT_NEGOCIO)
            }
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.nav_logout, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.itemId
        if (id == R.id.logout) {
            Toast.makeText(this, "Cerrando sesión", Toast.LENGTH_LONG).show()
            TokenPref(this).setToken("nan")
            TokenPref(this).setRole("CLIENT")
            TokenPref(this).setVentId("nan")
            TokenPref(this).clearProdCreate()
            TokenPref(this).clearVentCreate()
            finish()
            return true
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.home_activity)

        toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)

        val navigation = findViewById<View>(R.id.navigation) as BottomNavigationView
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)
        val ft: FragmentTransaction = supportFragmentManager.beginTransaction()
        ft.add(R.id.fragment_container, HomeFragment.newInstance(), FRAGMENT_HOME)
            .commit()

        var reciever = object : BroadcastReceiver() {
            override fun onReceive(p0: Context, p1: Intent) {
                if(!CheckConnectivity.checkConnectivity(p0)){
                    displaySnackbar()
                }
            }
        }
        registerReceiver(reciever, IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION))


        if(!CheckConnectivity.checkConnectivity(this)){
            displaySnackbar()
        }

    }


    override fun onBackPressed() {
        moveTaskToBack(true)
    }

    private fun displaySnackbar() {
        val snackBar = Snackbar.make(
                container, "There is no internet connection",
                Snackbar.LENGTH_LONG
        )
        snackBar.show()
    }


    private fun replaceFragment(newFragment: Fragment, tag: String) {
        val ft: FragmentTransaction = supportFragmentManager.beginTransaction()
        ft.replace(R.id.fragment_container, newFragment, tag)
            .commit()
    }

    companion object {
        const val FRAGMENT_HOME = "FRAGMENT_HOME"
        const val FRAGMENT_ACTIVIDAD = "FRAGMENT_ACTIVIDAD"
        const val FRAGMENT_EXPLORA = "FRAGMENT_EXPLORA"
        const val FRAGMENT_NEGOCIO = "FRAGMENT_NEGOCIO"
    }
}