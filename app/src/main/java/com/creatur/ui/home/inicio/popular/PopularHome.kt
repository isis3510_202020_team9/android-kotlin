package com.creatur.ui.home.inicio.popular
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.creatur.R
import com.creatur.data.product.Product
import com.creatur.data.venture.Venture
import com.creatur.ui.home.HomeViewModel
import com.creatur.ui.login.TokenPref
import com.creatur.utilities.InjectorUtils


/**
 * A simple [Fragment] subclass.
 */
class PopularHome : Fragment() {

    private lateinit var viewModel: HomeViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?

    ): View? {
        val view = inflater.inflate(R.layout.fragment_popular_home, container, false)

        viewModel = activity?.run {
            ViewModelProvider(this, InjectorUtils.provideHomeViewModelFactory(this))
                .get(HomeViewModel::class.java)
        } ?: throw Exception("Error con activity principal de Home")

        val preference = TokenPref(this.context?:throw Exception("Error de context"))
        var dataVentures: List<Venture>
        var dataProducts: List <Product>

        viewModel.popularVenture(preference.getToken()?:"nan")
        viewModel.popularProduct(preference.getToken()?:"nan")

        val layoutManagerVenture = LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false)

        val myListVentures = view.findViewById(R.id.VenturesRecyclerView) as RecyclerView
        viewModel.popularVentures.observe(viewLifecycleOwner, Observer<List<Venture>> { result ->
            result?.apply {
                dataVentures = result
                val adapterVentures = VentureAdapterPopular(dataVentures)
                myListVentures.adapter = adapterVentures
            }
        })
        myListVentures.layoutManager = layoutManagerVenture

        val layoutManagerProduct = LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false)

        val myListProducts = view.findViewById(R.id.ProductsRecyclerView) as RecyclerView
        viewModel.popularProducts.observe(viewLifecycleOwner, Observer<List<Product>>{
                result -> result?.apply{
            dataProducts = result
            val adapterProducts = ProductAdapterPopular(dataProducts)
            myListProducts.adapter = adapterProducts
        }
        })
        myListProducts.layoutManager = layoutManagerProduct

        return view
    }
}