package com.creatur.ui.home.explora

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import com.creatur.R
import com.creatur.data.product.Product
import com.creatur.data.venture.Venture
import com.creatur.databinding.ActivityVentureDetailBinding
import com.creatur.databinding.FragmentCategoryBinding
import com.creatur.ui.login.TokenPref
import com.creatur.ui.productDetail.ProductDetail
import com.creatur.ui.ventureDetail.ProductAdapterVenture
import com.creatur.ui.ventureDetail.VentureDetail
import com.creatur.ui.ventureDetail.VentureDetailViewModel
import com.creatur.utilities.InjectorUtils


class CategoryActivity : AppCompatActivity(), ProductAdapterCategory.OnProductClickListener, VentureAdapterCategory.OnVentureClickListener {
    private lateinit var viewModel: CategoryViewModel
    var category: String? = null
    var dataProducts: List <Product> = emptyList()
    var dataVentures: List <Venture> = emptyList()


    override fun onCreate(savedInstanceState: Bundle?) {
        category = intent.getStringExtra("category")
        super.onCreate(savedInstanceState)

        val preference = TokenPref(this)

        viewModel = this.run {
            ViewModelProvider(this, InjectorUtils.provideCategoryViewModelFactory(this))
                    .get(CategoryViewModel::class.java)
        }

        val binding = FragmentCategoryBinding.inflate(layoutInflater)

        if (category != null) {
            binding.categoria.text = category
            viewModel.productCategory(preference.getToken() ?: "nan", category!!)
            val layoutManagerProduct =
                LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
            val myListProducts = binding.ProductsRecyclerView
            viewModel.productslistcategory.observe(this, Observer<List<Product>> { result ->
                result?.apply {
                    dataProducts = result
                    val adapterProducts =
                        ProductAdapterCategory(dataProducts, this@CategoryActivity)
                    myListProducts.adapter = adapterProducts
                }
            })

            myListProducts.layoutManager = layoutManagerProduct

            viewModel.ventureCategory(preference.getToken() ?: "nan", category!!)
//            val layoutManagerVenture =
//                LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
            val myListVentures = binding.VenturesRecyclerView
            viewModel.ventureslistcategory.observe(this, Observer<List<Venture>> { result ->
                result?.apply {
                    dataVentures = result
                    val adapterVentures =
                        VentureAdapterCategory(dataVentures, this@CategoryActivity)
                    myListVentures.adapter = adapterVentures
                }
            })

            myListProducts.layoutManager = layoutManagerProduct

        }
        setContentView(binding.root)
    }
    override fun onProductClick(product: Product) {
        //llamar a una función en activity
        val showProductDetail = Intent (this, ProductDetail::class.java)
        showProductDetail.putExtra("producto", product)
        startActivity(showProductDetail)
    }


    override fun onVentureClick(venture: Venture) {
        val showVentureDetail = Intent (this, VentureDetail::class.java)
        showVentureDetail.putExtra("negocio", venture)
        startActivity(showVentureDetail)
    }


}
