package com.creatur.ui.home.explora

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.creatur.data.product.ProductRepository
import com.creatur.data.venture.VentureRepository


class CategoryViewModelFactory( private val productRepository: ProductRepository, private val ventureRepository: VentureRepository): ViewModelProvider.NewInstanceFactory() {
    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return CategoryViewModel(
            productRepository,
            ventureRepository
        ) as T
    }
}