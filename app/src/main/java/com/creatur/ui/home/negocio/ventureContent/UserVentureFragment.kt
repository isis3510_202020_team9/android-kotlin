package com.creatur.ui.home.negocio.ventureContent

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.viewpager.widget.ViewPager
import com.creatur.R
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.tabs.TabLayout

/**
 * A simple [Fragment] subclass.
 * Use the [UserVentureFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class UserVentureFragment : Fragment() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    private lateinit  var viewPager : ViewPager
    private lateinit  var tabs : TabLayout
    private lateinit  var button : FloatingActionButton


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view :  View  = inflater.inflate (R.layout.fragment_user_venture, container, false )
        viewPager = view.findViewById (R.id.tabs_viewpager_venture)
        tabs = view.findViewById (R.id.tab_layout_venture)

        val fragmentAdapter =  PagerAdapterVenture(childFragmentManager)
        viewPager.adapter = fragmentAdapter
        tabs.setupWithViewPager (viewPager)

        return view
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @return A new instance of fragment VentureFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance() =
            UserVentureFragment()
                .apply {
                }
    }
}