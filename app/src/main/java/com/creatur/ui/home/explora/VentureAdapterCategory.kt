package com.creatur.ui.home.explora

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.creatur.R
import com.creatur.data.venture.Venture

class VentureAdapterCategory (val lista: List<Venture>, private val listener: VentureAdapterCategory.OnVentureClickListener):
    RecyclerView.Adapter<VentureAdapterCategory.ViewHolder>() {
    inner class ViewHolder(val view: View): RecyclerView.ViewHolder(view) {
        init {
            view.setOnClickListener {
                val position: Int = adapterPosition
                if (position != RecyclerView.NO_POSITION) {
                    val venture = lista[position]
                    listener.onVentureClick(venture)
                }
            }
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.venture_item, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount() = lista.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val name = holder.view.findViewById<TextView>(R.id.text_view_venture_name)
        name.text = lista[position].name
        val url = lista[position].photo
        val photoItem = holder.view.findViewById<ImageView>(R.id.image_view)
        Glide.with(photoItem.context).load(url).centerCrop().fallback(R.drawable.ic_menu_camera).into(photoItem)
    }
    interface OnVentureClickListener{
        fun onVentureClick(venture: Venture)
    }
}