package com.creatur.ui.home.inicio.reccomended

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.creatur.R
import com.creatur.data.product.Product
import com.creatur.data.venture.Venture
import com.creatur.ui.home.HomeViewModel
import com.creatur.ui.login.TokenPref
import com.creatur.ui.productDetail.ProductDetail
import com.creatur.ui.ventureDetail.VentureDetail
import com.creatur.utilities.InjectorUtils


/**
 * A simple [Fragment] subclass.
 */
class RecommendedHome : Fragment(), ProductAdapterRecommended.OnProductClickListener, VentureAdapterRecommended.OnVentureClickListener {

    private lateinit var viewModel: HomeViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?

    ): View? {
        val view = inflater.inflate(R.layout.fragment_recommended_home, container, false)

        viewModel = activity?.run {
            ViewModelProvider(this, InjectorUtils.provideHomeViewModelFactory(view.context))
                .get(HomeViewModel::class.java)
        } ?: throw Exception("Error con activity principal de Home")

        val preference = TokenPref(this.context?:throw Exception("Error de context"))
        var dataVentures: List<Venture>
        var dataProducts: List <Product>


        viewModel.suggestedVenture(preference.getToken()?:"nan")
        viewModel.suggestedProduct(preference.getToken()?:"nan")


        val layoutManagerProduct = LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false)

        val myListProducts = view.findViewById(R.id.ProductsRecyclerView) as RecyclerView

        viewModel.suggestedProducts.observe(viewLifecycleOwner, Observer<List<Product>>{
                result -> result?.apply{
            dataProducts = result
            val adapterProducts = ProductAdapterRecommended(dataProducts, this@RecommendedHome)
            myListProducts.adapter = adapterProducts
        }
        })

        myListProducts.layoutManager = layoutManagerProduct

        val layoutManagerVenture = LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false)

        val myListVentures = view.findViewById(R.id.VenturesRecyclerView) as RecyclerView

        viewModel.suggestedVentures.observe(viewLifecycleOwner, Observer<List<Venture>> { result ->
            result?.apply {
                dataVentures = result
                val adapterVentures = VentureAdapterRecommended(dataVentures, this@RecommendedHome)
                myListVentures.adapter = adapterVentures
            }
        })
        myListVentures.layoutManager = layoutManagerVenture
        return view
    }

   override fun onProductClick(product: Product) {

        //llamar a una función en activity
       val showProductDetail = Intent (getActivity(), ProductDetail::class.java)
       showProductDetail.putExtra("producto", product)
       startActivity(showProductDetail)
    }

    override fun onVentureClick(venture: Venture) {

        //llamar a una función en activity
        val showVentureDetail = Intent (getActivity(), VentureDetail::class.java)
        showVentureDetail.putExtra("negocio", venture)
        startActivity(showVentureDetail)
    }


}