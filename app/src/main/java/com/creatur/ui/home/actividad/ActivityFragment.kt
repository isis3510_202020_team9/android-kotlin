package com.creatur.ui.home.actividad

import android.content.Intent
import android.graphics.Rect
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.creatur.R
import com.creatur.data.order.OrderStatus
import com.creatur.ui.home.HomeViewModel
import com.creatur.ui.login.TokenPref
import com.creatur.ui.orderStatus.OrderStatusActivity
import com.creatur.utilities.CheckConnectivity
import com.creatur.utilities.InjectorUtils

/**
 * A simple [Fragment] subclass.
 * Use the [ActivityFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class ActivityFragment : Fragment(), ActivityOrdersRecyclerViewAdapter.OnOrderClickListener {

    private lateinit var viewModel: HomeViewModel
    private var columnCount = 1


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        arguments?.let {
            columnCount = it.getInt(ARG_COLUMN_COUNT)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_activity, container, false)
        val recycler = view.findViewById<RecyclerView>(R.id.list)

        viewModel = activity?.run {
            ViewModelProvider(this, InjectorUtils.provideHomeViewModelFactory(view.context))
                    .get(HomeViewModel::class.java)
        } ?: throw Exception("Error con activity principal de Home")

        if(!CheckConnectivity.checkConnectivity(activity?:throw Exception("Error con activity principal de Home"))){
            Toast.makeText(activity, "No hay conexión a internet", Toast.LENGTH_LONG).show()
        }

        viewModel.listUserOrders(TokenPref(this.context?:throw Exception("Error de context")).getToken()?:"nan")

        viewModel.userOrders.observe(viewLifecycleOwner, {
            result -> result.apply {
                if(recycler is RecyclerView){
                    recycler.addItemDecoration(MarginItemDecoration(2))
                    with(recycler) {
                        layoutManager = LinearLayoutManager(context)

                        adapter = ActivityOrdersRecyclerViewAdapter(result, this@ActivityFragment)
                    }
                }
        }
        })

        return view
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @return A new instance of fragment ActivityFragment.
         */
        // TODO: Rename and change types and number of parameters
        const val ARG_COLUMN_COUNT = "column-count"
        @JvmStatic
        fun newInstance() =
            ActivityFragment().apply {
            }

    }

    override fun onOrderClick(order: OrderStatus) {
        val intent = Intent (activity, OrderStatusActivity::class.java)
        intent.putExtra("productOrderStatus", order)
        intent.putExtra("type", 2)
        startActivity(intent)
    }
}

class MarginItemDecoration(private val spaceHeight: Int) : RecyclerView.ItemDecoration() {
    override fun getItemOffsets(outRect: Rect, view: View,
                                parent: RecyclerView, state: RecyclerView.State) {
        with(outRect) {
            if (parent.getChildAdapterPosition(view) == 0) {
                top = spaceHeight
            }
            left =  spaceHeight
            right = spaceHeight
            bottom = spaceHeight
        }
    }
}