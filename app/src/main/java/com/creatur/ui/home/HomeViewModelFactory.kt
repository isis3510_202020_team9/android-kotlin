package com.creatur.ui.home

import android.content.Context
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.creatur.data.product.ProductRepository
import com.creatur.data.venture.VentureRepository

/* Class that represents a Factory of ViewModels, basically an abstract class that handles any HomeViewModel as generic */
class HomeViewModelFactory(private val application: Context, private val ventureRepository: VentureRepository, private val productRepository: ProductRepository): ViewModelProvider.NewInstanceFactory() {
    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return HomeViewModel(
                application, ventureRepository, productRepository
        ) as T
    }
}