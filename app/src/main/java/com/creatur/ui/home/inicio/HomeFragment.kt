package com.creatur.ui.home.inicio

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.viewpager.widget.PagerAdapter

import androidx.viewpager.widget.ViewPager
import com.creatur.R
import com.google.android.material.tabs.TabLayout


/**
 * A simple [Fragment] subclass.
 * Use the [BlankFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class HomeFragment : Fragment() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    private  lateinit  var viewPager : ViewPager
    private  lateinit  var tabs : TabLayout


    override  fun  onCreateView ( inflater :  LayoutInflater , container :  ViewGroup ? ,
                                  savedInstanceState :  Bundle ? ): View ? {
        // Inflate the layout for this fragment
        val view :  View  = inflater.inflate (R.layout.fragment_inicio, container, false )
        viewPager = view.findViewById (R.id.tabs_viewpager)
        tabs = view.findViewById (R.id.tab_layout)

        val fragmentAdapter =  MyPagerAdapter(childFragmentManager)
        viewPager.adapter = fragmentAdapter
        tabs.setupWithViewPager (viewPager)

        return view
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @return A new instance of fragment BlankFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance() =
            HomeFragment().apply {
            }
    }
}