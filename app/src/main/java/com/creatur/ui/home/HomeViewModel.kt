package com.creatur.ui.home

import android.app.Application
import android.content.Context
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.creatur.data.DatabaseLocal
import com.creatur.data.order.Order
import com.creatur.data.order.OrderStatus
import com.creatur.data.product.Product
import com.creatur.data.product.ProductRepository
import com.creatur.data.venture.Near
import com.creatur.data.venture.Venture
import com.creatur.data.venture.VentureRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.lang.Exception

/*Class that represents a View Model for the home page*/
class HomeViewModel(application: Context, private val ventureRepository: VentureRepository, private val productRepository: ProductRepository): ViewModel() {
    val suggestedVentures = MutableLiveData<List<Venture>>()
    val suggestedProducts = MutableLiveData<List<Product>>()
    val nearVentures = MutableLiveData<List<Venture>>()
    val nearProducts = MutableLiveData<List<Product>>()
    val popularVentures = MutableLiveData<List<Venture>>()
    val popularProducts = MutableLiveData<List<Product>>()
    val ventureOrders = MutableLiveData<List<OrderStatus>>()
    val ventureProducts = MutableLiveData<List<Product>>()
    val userOrders = MutableLiveData<List<OrderStatus>>()
    val myvent = MutableLiveData<Venture>()

    init {
        val sugProdsDAO = DatabaseLocal.getInstance(application).productDao
        productRepository.productDAO = sugProdsDAO
        myvent.value = null
    }

    fun suggestedVenture(token: String) = viewModelScope.launch(Dispatchers.IO) {
        try{
            suggestedVentures.postValue(ventureRepository.suggest(token))
        } catch (e: Exception){

        }
    }

    fun suggestedProduct(token:String ) = viewModelScope.launch(Dispatchers.IO) {
        try{
            suggestedProducts.postValue(productRepository.suggest(token))
        } catch (e: Exception){

        }
    }

    fun near(lat: Double, lon: Double, token: String) = viewModelScope.launch(Dispatchers.IO) {
        try{
            val near: Near = ventureRepository.near(token, lat, lon)
            nearVentures.postValue(near.nearVentures)
            nearProducts.postValue(near.nearProducts)
        } catch (e: Exception){

        }
    }

    fun myvent(token:String) = viewModelScope.launch(Dispatchers.IO) {
        try{
            myvent.postValue(ventureRepository.myvent(token))
        } catch (e: Exception){

        }
    }

    fun popularVenture(token: String) = viewModelScope.launch(Dispatchers.IO) {
        try{
            popularVentures.postValue(ventureRepository.suggest(token))
        } catch (e: Exception){

        }
    }

    fun popularProduct(token:String ) = viewModelScope.launch(Dispatchers.IO) {
        try{
            popularProducts.postValue(productRepository.suggest(token))
        } catch (e: Exception){

        }
    }

    fun listVentureOrders(token:String) = viewModelScope.launch(Dispatchers.IO) {
        try{
            ventureOrders.postValue(ventureRepository.getVentureOrders(token))
        } catch (e: Exception){

        }
    }

    fun listVentureProducts(token:String, ventid: String) = viewModelScope.launch(Dispatchers.IO) {
        try{
            ventureProducts.postValue(ventureRepository.getVentureProducts(token, ventid))
        } catch (e: Exception){

        }
    }

    fun listUserOrders(token: String) = viewModelScope.launch(Dispatchers.IO) {
        try{
            userOrders.postValue(ventureRepository.getUserOrders(token))
        } catch (e: Exception){

        }
    }
}