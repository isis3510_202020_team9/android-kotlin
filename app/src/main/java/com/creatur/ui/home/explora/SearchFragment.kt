package com.creatur.ui.home.explora

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.ViewModelProvider
import com.creatur.R
import com.creatur.databinding.ActivityVentureDetailBinding
import com.creatur.databinding.FragmentSearchBinding
import com.creatur.ui.home.HomeViewModel
import com.creatur.ui.login.TokenPref
import com.creatur.ui.productDetail.ProductDetail
import com.creatur.utilities.InjectorUtils

/**
 * A simple [Fragment] subclass.
 * Use the [SearchFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class SearchFragment : Fragment() {
    var categoria: String? = null;
    private lateinit var viewModel: HomeViewModel


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_search, container, false)
        viewModel = activity?.run {
            ViewModelProvider(this, InjectorUtils.provideHomeViewModelFactory(view.context))
                .get(HomeViewModel::class.java)
        } ?: throw Exception("Error con activity principal de Home")
//        val preference = TokenPref(this.context?:throw Exception("Error de context"))
        val binding = FragmentSearchBinding.inflate(layoutInflater)


        binding.buttonComida.setOnClickListener{
            Log.d("ejemplo","acaComida")
            categoria = "FAST_FOOD";
            //llamar a una función en activity
            val catActivity = Intent (activity, CategoryActivity::class.java)
            catActivity.putExtra("category", categoria)
            activity?.startActivity(catActivity)

        }
        binding.buttonDeportes.setOnClickListener{
            categoria = "SPORTS";
            //llamar a una función en activity
            val catActivity = Intent (getActivity(), CategoryActivity::class.java)
            catActivity.putExtra("category", categoria)
            getActivity()?.startActivity(catActivity)
        }
        binding.buttonJuguetes.setOnClickListener{
            categoria = "TOYS";
            //llamar a una función en activity
            val catActivity = Intent (getActivity(), CategoryActivity::class.java)
            catActivity.putExtra("category", categoria)
            getActivity()?.startActivity(catActivity)
        }
        binding.buttonMascotas.setOnClickListener{
            categoria = "PETS";
            //llamar a una función en activity
            val catActivity = Intent (getActivity(), CategoryActivity::class.java)
            catActivity.putExtra("category", categoria)
            getActivity()?.startActivity(catActivity)
        }
        binding.buttonReposteria.setOnClickListener{
            categoria = "BAKERY";
            //llamar a una función en activity
            val catActivity = Intent (getActivity(), CategoryActivity::class.java)
            catActivity.putExtra("category", categoria)
            getActivity()?.startActivity(catActivity)
        }
        binding.buttonRopa.setOnClickListener{
            categoria = "CLOTHING";
            //llamar a una función en activity
            val catActivity = Intent (getActivity(), CategoryActivity::class.java)
            catActivity.putExtra("category", categoria)
            getActivity()?.startActivity(catActivity)
        }


        return view
    }



    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @return A new instance of fragment SearchFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance() =
            SearchFragment().apply {
            }

    }
}