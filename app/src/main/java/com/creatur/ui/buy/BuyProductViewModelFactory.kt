package com.creatur.ui.buy

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.creatur.data.order.OrderRepository

class BuyProductViewModelFactory(private val orderRepository: OrderRepository): ViewModelProvider.NewInstanceFactory() {
    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return BuyProductViewModel(
            orderRepository
        ) as T
    }
}
