package com.creatur.ui.buy

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.creatur.data.order.Order
import com.creatur.data.order.OrderRepository
import com.creatur.data.order.OrderStatus
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class BuyProductViewModel(private val orderRepository: OrderRepository): ViewModel() {

    val orderStatus = MutableLiveData<OrderStatus>()

    init {
        orderStatus.value = OrderStatus("","",0.0,null,"",status = "none",createdAt = "")
    }

    fun buy(token: String, order: Order) = viewModelScope.launch(Dispatchers.IO) {
        try {
            orderStatus.postValue(orderRepository.buy(token, order))
        }
        catch (e: Exception){
            orderStatus.postValue(OrderStatus("","",0.0,null,"",status = "error",createdAt = ""))
        }
    }
}