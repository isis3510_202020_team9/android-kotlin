package com.creatur.ui.buy

import android.app.DatePickerDialog
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import com.bumptech.glide.Glide
import com.creatur.R
import com.creatur.data.order.Order
import com.creatur.data.order.OrderStatus
import com.creatur.data.order.TmpProduct
import com.creatur.data.product.Product
import com.creatur.ui.login.TokenPref
import com.creatur.utilities.InjectorUtils
import kotlinx.android.synthetic.main.activity_buy_product.*
import java.util.*
import androidx.lifecycle.Observer
import com.creatur.ui.orderStatus.OrderStatusActivity
import com.creatur.utilities.CheckConnectivity
import com.google.android.material.snackbar.Snackbar

class BuyProductActivity : AppCompatActivity(), DatePickerDialog.OnDateSetListener {

    private lateinit var viewModel: BuyProductViewModel
    private var y = 0
    private var m = 0
    private var d = 0
    private lateinit var editdate: EditText
    private lateinit var counter: TextView
    private lateinit var total: TextView
    private var prc = 0
    private var cantidad = 1
    private var producto: Product? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        producto = intent.getParcelableExtra<Product>("productoBuy")
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_buy_product)
        if(producto != null){
            nombre.text = producto!!.name
            Glide.with(this).load(producto!!.photo).centerCrop().fallback(R.drawable.ic_menu_camera).into(ventureImage)
            Glide.with(this).load(producto!!.photo).centerCrop().fallback(R.drawable.ic_menu_camera).into(productImage)
            precio.text = "$" + producto!!.prize.toString()
            prc = producto!!.prize
            preciototal.text = "$" + producto!!.prize.toString()
        }

        viewModel = ViewModelProvider(this, InjectorUtils.provideNewBuyProductViewModelFactory())
            .get(BuyProductViewModel::class.java)

        viewModel.orderStatus.observe(this, Observer<OrderStatus> { result ->
            result?.apply {
                if(result.status != "none"){
                    if(result.status == "error"){
                        Toast.makeText(this@BuyProductActivity, "Ha ocurrido un error con la compra", Toast.LENGTH_LONG).show()
                    }
                    else{
                        Toast.makeText(this@BuyProductActivity, "Compra finalizada", Toast.LENGTH_LONG).show()
                        val intent = Intent(this@BuyProductActivity, OrderStatusActivity::class.java)
                        intent.putExtra("productOrder", producto)
                        intent.putExtra("productOrderStatus", result)
                        startActivity(intent)
                        finish()
                    }
                }
            }
        })

        editdate = findViewById(R.id.editdate)
        editdate.keyListener = null

        counter = findViewById(R.id.counter)
        counter.text = "1"

        total = findViewById(R.id.preciototal)

        val c = Calendar.getInstance()
        y = c.get(Calendar.YEAR)
        m = c.get(Calendar.MONTH)
        d = c.get(Calendar.DAY_OF_MONTH)

        pickDate()
        setCounter()
        send()

    }

    private fun send() {
        val but = findViewById<Button>(R.id.button_buy)
        val dir = findViewById<EditText>(R.id.editdir)
        but.setOnClickListener {
            val date = editdate.text.toString()
            val dirtxt = dir.text.toString()
            if(date.isEmpty()){
                Toast.makeText(this, "Fecha inválida", Toast.LENGTH_LONG).show()
                return@setOnClickListener
            }
            if(dirtxt.isEmpty()){
                Toast.makeText(this, "Dirección inválida", Toast.LENGTH_LONG).show()
                return@setOnClickListener
            }
            if(cantidad == 0){
                Toast.makeText(this, "Cantidad inválida", Toast.LENGTH_LONG).show()
                return@setOnClickListener
            }
            val tmpProduct = mutableListOf<TmpProduct>(TmpProduct(producto?.id,cantidad))
            val order = Order(
                tmpProduct,
                producto!!.venture?.id?:"nan",
                dirtxt,
                "$y-$m-${d}T00:00:00.000"
            )
            if(!CheckConnectivity.checkConnectivity(this)){
                displaySnackbar()
            }
            else{
                viewModel.buy(TokenPref(this).getToken()?:"nan", order)
            }
        }
    }

    private fun setCounter() {
        val up = findViewById<Button>(R.id.buttonup)
        val down = findViewById<Button>(R.id.buttondown)

        up.setOnClickListener {
            if (cantidad < 20){
                cantidad++
                counter.text = "$cantidad"
                total.text = (cantidad*prc).toString()
            }
        }

        down.setOnClickListener {
            if (cantidad > 1) {
                cantidad--
                counter.text = "$cantidad"
                total.text = (cantidad*prc).toString()
            }
        }
    }

    private fun pickDate() {
        editdate.setOnClickListener {
            DatePickerDialog(this, this, y, m, d).show()
        }
    }

    override fun onDateSet(p0: DatePicker?, p1: Int, p2: Int, p3: Int) {
        y = p1
        m = p2
        d = p3
        val valor = "$y/$m/$d"
        editdate.setText(valor)
    }

    private fun displaySnackbar() {
        val snackBar = Snackbar.make(
                container, "There is no internet connection, check your connection please",
                Snackbar.LENGTH_LONG
        )
        snackBar.show()
    }
}