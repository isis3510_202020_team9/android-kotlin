package com.creatur.ui.newProductFormActivity

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Spinner
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.creatur.R
import com.creatur.data.product.Product
import com.creatur.ui.login.TokenPref
import com.creatur.utilities.CheckConnectivity
import com.creatur.utilities.InjectorUtils
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.activity_new_product_form.*

class NewProductFormActivity : AppCompatActivity() {
    private lateinit var viewModel: NewProductViewModel
    private lateinit var token: String
    private lateinit var final: Product

    private var REQUEST_OK = 200
    private var REQUEST_CODE = 190

    private val cats = mutableListOf("CLOTHING","FAST_FOOD","PETS","TOYS","BAKERY","SPORTS")

    private var cat : Int = -1

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_new_product_form)

        val cached = TokenPref(this).getProdCreate()

        if (cached.isNotEmpty() && cached.elementAt(0) != "nan"){
            editnombre.setText(cached.elementAt(0))
            editprecio.setText(cached.elementAt(1))
            editdesc.setText(cached.elementAt(2))
            cat = cached.elementAt(3).toInt()
        }

        if(!CheckConnectivity.checkConnectivity(this)){
            displaySnackbar()
        }

        val preference = TokenPref(this)
        token = preference.getToken()?:"nan"

        viewModel = ViewModelProvider(this, InjectorUtils.provideNewProductViewModelFactory())
            .get(NewProductViewModel::class.java)

        initializeUi()
        defineSpinner()

        viewModel.resp.observe(this, Observer<String> { result ->
            result?.apply {
                if( result != "none"){
                    if(result == "done"){
                        val dat = Intent()
                        dat.putExtra("product", final)
                        setResult(REQUEST_OK, dat)
                        finish()
                    }
                    else {
                        Toast.makeText(this@NewProductFormActivity, result, Toast.LENGTH_LONG).show()
                    }
                }
            }
        })

    }

    private fun initializeUi() {
        button_crear.setOnClickListener {
            val nom = editnombre.text.toString()
            val pre = editprecio.text.toString()
            val desc = editdesc.text.toString()
            if (nom.isEmpty() || nom.length>50){
                Toast.makeText(this, "El nombre no puede ser vacio ni ser mayor a 50 caracteres", Toast.LENGTH_LONG).show()
                return@setOnClickListener
            }
            if (pre.isEmpty()){
                Toast.makeText(this, "El precio no puede ser vacio", Toast.LENGTH_LONG).show()
                return@setOnClickListener
            }
            if (desc.isEmpty() || desc.length>400){
                Toast.makeText(this, "La descripción no puede ser vacia ni ser mayor a 400 caracteres", Toast.LENGTH_LONG).show()
                return@setOnClickListener
            }
            val newProduct = Product(
                name = nom,
                prize = pre.toInt(),
                description = desc,
                productCategory = cats[cat]
            )
            if(!CheckConnectivity.checkConnectivity(this)){
                displaySnackbar()
                TokenPref(this).setProdCreate(nom, pre, desc, cat.toString())
            }
            else{
                viewModel.create(token, newProduct)
                final = newProduct
                TokenPref(this).clearProdCreate()
            }
        }
    }

    private fun defineSpinner() {
        val spin : Spinner = findViewById(R.id.spin)
        if(cat != -1){
            spin.setSelection(cat)
        }
        val arr = resources.getStringArray(R.array.categorias)
        val ad = ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, arr)
        spin.adapter = ad
        spin.onItemSelectedListener = object :
            AdapterView.OnItemSelectedListener {
            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                cat = p2
            }

            override fun onNothingSelected(p0: AdapterView<*>?) {
                cat = 0
            }
        }
    }

    private fun displaySnackbar() {
        val snackBar = Snackbar.make(
            container, "No hay conexión a internet, tus datos se guardarán para la proxima vez que intentes crear un producto",
            Snackbar.LENGTH_LONG
        )
        snackBar.show()
    }
}