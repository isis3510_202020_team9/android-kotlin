package com.creatur.ui.newProductFormActivity

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.creatur.data.product.ProductRepository

class NewProductViewModelFactory(private val productRepository: ProductRepository): ViewModelProvider.NewInstanceFactory() {
    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return NewProductViewModel(
            productRepository
        ) as T
    }
}