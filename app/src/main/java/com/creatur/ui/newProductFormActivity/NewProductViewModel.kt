package com.creatur.ui.newProductFormActivity

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.creatur.data.product.Product
import com.creatur.data.product.ProductRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class NewProductViewModel(private val productRepository: ProductRepository): ViewModel() {
    val resp = MutableLiveData<String>()
    init {
        resp.value = "none"
    }

    fun create(token: String, product: Product) = viewModelScope.launch(Dispatchers.IO) {
        try {
            productRepository.create(token, product)
            resp.postValue("done")
        } catch (e: Exception){
            resp.postValue("La información es inválida, por favor revise lo ingresado.")
        }
    }
}